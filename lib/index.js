/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
const FlowLogger = require('./FlowLogger')
const semver = require('semver')
const os = require('os')
const Writer = require('./Writer')

/**
 * Initialize `flowlogger 𝕋流` with the `programName`, `programVersion`, `logDataVersion`, the {@link Writer} to use,
 * the machine host name, process id, and a UUID to identify the execution.
 *
 * As a side effect, log detailed information about the execution at {@link Level#INFO} with {@link Phase#INITIALIZED}.
 *
 * Return the function to use at the start of each called business function to get a flowlogger 𝕋流 instance.
 *
 * @constructor
 * @property {Contract} contract
 * @param {string} kwargs.programName - name of the program that uses `flowlogger 𝕋流`
 * @param {string} kwargs.programVersion - semantic version of the program that uses `flowlogger 𝕋流`
 * @param {string} kwargs.dataStructureVersion - semantic version of the structure of the values of the `data` property
 *                                               of the structured records that will be logged
 * @param {Writer} kwargs.writer - instance that will output structured log records
 */
const prepareExecutionFlowLogger = new Contract({
  pre: [
    kwargs => !!kwargs,
    kwargs => typeof kwargs === 'object',
    kwargs => !!kwargs.programName,
    kwargs => typeof kwargs.programName === 'string',
    kwargs => semver.valid(kwargs.programVersion),
    kwargs => semver.valid(kwargs.logDataVersion),
    kwargs => kwargs.writer instanceof Writer,
    () => !FlowLogger.executionLogger,
    () => !FlowLogger.current
  ],
  post: [
    () => FlowLogger.executionLogger instanceof FlowLogger,
    kwargs => FlowLogger.executionLogger.program.name === kwargs.programName,
    kwargs => FlowLogger.executionLogger.program.version === semver.valid(kwargs.programVersion),
    kwargs => FlowLogger.executionLogger.program.logDataVersion === semver.valid(kwargs.logDataVersion),
    () => FlowLogger.executionLogger.execution.hostname === os.hostname(),
    () => FlowLogger.executionLogger.execution.pid === process.pid,
    kwargs => FlowLogger.executionLogger.writer === kwargs.writer,
    () => !FlowLogger.current
  ]
}).implementation(function prepareExecutionFlowLogger (kwargs) {
  const 𝕋logger = new FlowLogger(kwargs)
  𝕋logger.initialized() // sets FlowLogger.executionLogger
})

module.exports = prepareExecutionFlowLogger
