/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Joi = require('./_util/Joi')

/**
 * @type {ProgramIdentification}
 */
const example = {
  name: 'my program',
  version: '142.143.144-dev.145',
  logDataVersion: '9.8.7-pre.89038592'
}

/**
 * Information about the program that uses `flowlogger 𝕋流`
 *
 * @typedef ProgramIdentification
 * @type {object}
 * @property {string} name - name of the program
 * @property {string} version - semantic version of the program
 * @property {string} logDataVersion - semantic version of the `data` in structured log records
 */

/** @type {ObjectSchema} */
const programInformation = Joi.object()
  .keys({
    name: Joi.string().required(),
    version: Joi.semver().required(),
    logDataVersion: Joi.semver().required()
  })
  .example(example)

module.exports = { schema: programInformation, example }
