/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @type Joi */ const Joi = require('@hapi/joi')
const Level = require('./Level')
const flowLoggerIdentification = require('./flowloggerIdentification')
const HrTime = require('./HrTime')
const Phase = require('./Phase')
const ProgramIdentification = require('./ProgramIdentification')
const ExecutionIdentification = require('./ExecutionIdentification')
const FlowIdentification = require('./FlowIdentification')

/**
 * @type {WriterMessage}
 */
const example = {
  level: Level.INFO,
  at: HrTime.example,
  '𝕋流': { ...flowLoggerIdentification },
  program: { ...ProgramIdentification.example },
  execution: { ...ExecutionIdentification.example },
  flow: FlowIdentification.example,
  // TODO add file name via module?
  function: 'doIt',
  phase: Phase.SELECTION,
  data: {
    someProperty: 'some data'
  }
}

/**
 * Instances are written to log storage. Instances must be JSON stringifiable, and thus cannot
 * contain circular structures.
 *
 * @typedef WriterMessage
 * @type {object}
 * @property {!Level} level
 * @property {!HrTime} at
 * @property {!FlowloggerIdentification} 𝕋流
 * @property {!ProgramIdentification} program
 * @property {!ExecutionIdentification} execution
 * @property {!Phase} phase
 * @property {!object} data
 */
const writerMessage = Joi.object()
  .keys({
    level: Level.schema.required(),
    at: HrTime.schema.required(),
    '𝕋流': Joi.object()
      .valid(flowLoggerIdentification)
      .required(),
    program: ProgramIdentification.schema.required(),
    execution: ExecutionIdentification.schema.required(),
    flow: FlowIdentification.schema, // MUDO forbidden when INITIALIZED, required otherwise
    function: Joi.string(), // MUDO forbidden when INITIALIZED, required otherwise
    phase: Phase.schema.required(),
    data: Joi.object().required()
  })
  .example(example)

module.exports = { schema: writerMessage, example }
