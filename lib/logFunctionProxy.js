/*
 * Copyright 2020 - 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('./_util/validateSchema')
const FlowIdentification = require('./FlowIdentification').schema
const FlowLogger = require('./FlowLogger')
const isArguments = require('./_util/isArguments')

// hack to get old values
const createLoggerOldStack = []

const createLogger = new Contract({
  pre: [
    () => {
      if (createLogger.contract.verifyPostconditions) {
        // hack to get old values
        createLoggerOldStack.unshift({
          'FlowLogger.executionLogger': FlowLogger.executionLogger,
          'FlowLogger.current': FlowLogger.current
        })
      }
      return true
    },
    functionName => !!functionName && typeof functionName === 'string',
    () => FlowLogger.executionLogger instanceof FlowLogger,
    (functionName, flowIdentification) => validateSchema(FlowIdentification, flowIdentification),
    () => !FlowLogger.current || !!FlowLogger.current.flow,
    (functionName, flowIdentification) => !!flowIdentification || FlowLogger.current instanceof FlowLogger
  ],
  post: [
    () => FlowLogger.executionLogger === createLoggerOldStack[0]['FlowLogger.executionLogger'],
    () => !!FlowLogger.current,
    function () {
      return Contract.outcome(arguments) === FlowLogger.current
    },
    function (functionName, flowIdentification) {
      return (
        Object.getPrototypeOf(FlowLogger.current) ===
        (!flowIdentification || arguments.length < 4 // 2 possible arguments, the result, and the bound contract function
          ? createLoggerOldStack[0]['FlowLogger.current']
          : createLoggerOldStack[0]['FlowLogger.executionLogger'])
      )
    },
    () => FlowLogger.current.flow,
    function (functionName, flowIdentification) {
      return (
        FlowLogger.current.flow ===
        (!flowIdentification || arguments.length < 4 // 2 possible arguments, the result, and the bound contract function
          ? createLoggerOldStack[0]['FlowLogger.current'].flow
          : flowIdentification)
      )
    },
    () => {
      // hack to release old values
      createLoggerOldStack.shift()
      return true
    }
  ]
}).implementation(function createLogger (functionName, flowIdentification) {
  /* prettier-ignore */
  const fresh = !flowIdentification
    ? Object.create(FlowLogger.current, {
      function: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: functionName
      }
    })
    : Object.create(FlowLogger.executionLogger, {
      flow: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: flowIdentification
      },
      function: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: functionName
      }

    })
  FlowLogger.current = fresh
  return fresh
})

const RequiredFlowIdentification = FlowIdentification.required()

/**
 * If provided, is called with the `this` and arguments the subject function is called with, and should return the
 * {@link FlowIdentification} of the new flow to log for. The idea is that this function extracts the
 * {@link FlowIdentification} from the arguments of the function where a flow enters the execution.
 */
const getFlowIdentificationContract = new Contract({
  post: [
    function () {
      return validateSchema(RequiredFlowIdentification, Contract.outcome(arguments))
    }
  ]
})
// since this is a hook method
getFlowIdentificationContract.verifyPostconditions = true

/* NOTE: `apply` and `construct` are hook methods for the JavaScript `Proxy` framework. As such we know for sure
         preconditions are met, and no postconditions are imposed.
         -
         We depend on the precondition that `target` is the logging subject function, that the proxy is called
         with `args`, and, in the case of `apply`, with the `this` of the call as `self`.
         -
         We extend the JavaScript `Proxy` framework contracts for the hook methods with the postconditions:
         - apply:
           - create a new FlowLogger instance with `FlowLogger.current` as prototype
           - log the underlying function being called on the new `FlowLogger` instance
           - call the underlying function with the arguments the `Proxy` is called with, plus the new `FlowLogger`
             instance as last argument
           - log the outcome of the underlying function on the new FlowLogger instance
           - reset `FlowLogger.current` to the prototype of new FlowLogger instance
           - return the result or rethrow the error of the underlying function
           - if the result is `PromiseLike`, when the `Promise` settles,
             - log the outcome of the underlying function on the new FlowLogger instance
             - reset `FlowLogger.current` to the prototype of new FlowLogger instance
             - resolve to the result or reject with the error of the underlying function
         -
         By the time the `Proxy` returns, `FlowLogger.current` is reset, and the call of the underlying function is
         done. Logging is transparent. There is little we can meaningfully express in postconditions, and preconditions
         are not necessary. Therefor, this function will be tested classically, without a `Contract`.
         */

/**
 * When `apply` is called,
 * - a new {@link FlowLogger} is created
 *   - with {@link FlowLogger#executionLogger} as prototype if `getFlowIdentification` is provided
 *   - with {@link FlowLogger#current} as prototype if `getFlowIdentification` is not provided
 * - {@link FlowLogger#current} is set to the new {@link FlowLogger}
 * - the call to the function is logged with {@link Phase#CALLED} on the new {@link FlowLogger}, with the provided
 *   `arguments`
 * - `target` is called, with the provided `arguments`
 *
 * Call with `getFlowIdentification` to decorate functions where a flow enters the execution. Call without
 * `getFlowIdentification` to decorate functions that are part of an existing flow.
 *
 * When the function call ends
 * - {@link FlowLogger#current} is (re)set to the prototype of the new {@link FlowLogger}
 * - if the function returns
 *   - if the ending is {@link Promise}-like, the return is logged with {@link Phase#WAITING}
 *   - if the ending is not {@link Promise}-like, the result and resulting value is logged with
 *     {@link Phase#RETURNS}
 * - if the function throws, the ending is logged with {@link Phase#THROWS}
 * - if a {@link Promise}-like result was returned, when it settles,
 *   - if the {@link Promise}-like result resolves, the resolution is logged with {@link Phase#RETURNS}
 *   - if the {@link Promise}-like result rejects, the rejection is logged with {@link Phase#THROWS}
 *
 * During execution of the function, anywhere in the body, {@link FlowLogger#current} refers to the new
 * {@link FlowLogger}, because
 * - it is set that way at the start
 * - it is reset when any logging function call in the body ends
 * - it is reset when any async logging function call in the body settles
 *
 * // MUDO find a way to determine when or how to log {@link Phase#FATAL}
 * // MUDO at this time, everyting is logged at level INFO; do something about that
 *
 * @param {function} [getFlowIdentification] - Optional function. If provided, is called with the `this` and arguments
 *                                             the subject function is called with, and should return the
 *                                             {@link FlowIdentification} of the new flow to log for. The idea is that
 *                                             this function extracts the {@link FlowIdentification} from the arguments
 *                                             of the function where a flow enters the execution. Must be an
 *                                             implementation that adheres to {@link getFlowIdentificationContract}.
 * @param {function} target - The decorated function
 * @param {function} [self] - `this` on which decorated `target` is called
 * @param {Array<*>} [args] - `arguments` of the call of decorated `target`; `undefined` if called without arguments
 * @return {*}
 */
const apply = new Contract({
  pre: [
    getFlowIdentification =>
      getFlowIdentification === undefined || getFlowIdentificationContract.isImplementedBy(getFlowIdentification),
    (getFlowIdentification, target) => typeof target === 'function',
    (getFlowIdentification, target, self) => self === undefined || (typeof self === 'object' && self !== null),
    (getFlowIdentification, target, self, args) => args === undefined || isArguments(args) || Array.isArray(args)
  ]
  // no meaningful postconditions
}).implementation(function apply (getFlowIdentification, target, self, args) {
  const 𝕋logger = createLogger(target.name, getFlowIdentification ? getFlowIdentification.apply(self, args) : undefined)
  𝕋logger.called(args)

  try {
    const result = target.apply(self, args)
    if (!result || !(typeof result.then === 'function' && typeof result.catch === 'function')) {
      return 𝕋logger.returns(result)
    }
    return 𝕋logger
      .waiting(result)
      .catch(err => {
        throw 𝕋logger.throws(err)
      })
      .then(result => {
        return 𝕋logger.returns(result)
      })
      .finally(() => {
        𝕋logger.resetCurrent()
      })
  } catch (err) {
    throw 𝕋logger.throws(err)
  } finally {
    𝕋logger.resetCurrent()
  }
})

/**
 * When `construct` is called,
 * - a new {@link FlowLogger} is created
 *   - with {@link FlowLogger#executionLogger} as prototype if `getFlowIdentification` is provided
 *   - with {@link FlowLogger#current} as prototype if `getFlowIdentification` is not provided
 * - {@link FlowLogger#current} is set to the new {@link FlowLogger}
 * - the call to the function is logged with {@link Phase#CALLED} on the new {@link FlowLogger}, with the provided
 *   `arguments`
 * - a new object is created with `target.prototype` as prototype
 * - `target` is called, with the provided `arguments` on the new object, to initialize it
 *
 * Call with `getFlowIdentification` to decorate functions where a flow enters the execution. Call without
 * `getFlowIdentification` to decorate functions that are part of an existing flow.
 *
 * A constructor cannot sensibly be async.
 *
 * When the function call ends
 * - {@link FlowLogger#current} is (re)set to the prototype of the new {@link FlowLogger}
 * - if the function returns, the return is logged with {@link Phase#WAITING}
 * - if the function throws, the ending is logged with {@link Phase#THROWS}
 *
 * During execution of the function, anywhere in the body, {@link FlowLogger#current} refers to the new
 * {@link FlowLogger}, because
 * - it is set that way at the start
 * - it is reset when any logging function call in the body ends
 * - it is reset when any async logging function call in the body settles
 *
 * // MUDO find a way to determine when or how to log {@link Phase#FATAL}
 * // MUDO at this time, everyting is logged at level INFO; do something about that
 *
 * @param {function} [getFlowIdentification] - Optional function. If provided, is called with the `this` and arguments
 *                                             the subject function is called with, and should return the
 *                                             {@link FlowIdentification} of the new flow to log for. The idea is that
 *                                             this function extracts the {@link FlowIdentification} from the arguments
 *                                             of the function where a flow enters the execution. Must be an
 *                                             implementation that adheres to {@link getFlowIdentificationContract}.
 * @param {function} target - The decorated function
 * @param {Array<*>} [args] - `arguments` of the call of decorated `target`; `undefined` if called without arguments
 * @return {*}
 */
const construct = new Contract({
  pre: [
    getFlowIdentification =>
      getFlowIdentification === undefined || getFlowIdentificationContract.isImplementedBy(getFlowIdentification),
    (getFlowIdentification, target) => typeof target === 'function',
    (getFlowIdentification, target, args) => args === undefined || isArguments(args) || Array.isArray(args)
  ]
  // no meaningful postconditions
}).implementation(function construct (getFlowIdentification, target, args) {
  const 𝕋logger = createLogger(
    target.name,
    getFlowIdentification ? getFlowIdentification.apply(undefined, args) : undefined
  )
  𝕋logger.called(args)

  const self = Object.create(target.prototype)

  try {
    target.apply(self, args)
    return 𝕋logger.returns(self)
  } catch (err) {
    throw 𝕋logger.throws(err)
  } finally {
    𝕋logger.resetCurrent()
  }
})

/**
 * Returns a Proxy for a function (that can be used as a regular function or a constructor).
 *
 * When this proxy is called,
 * - a new {@link FlowLogger} is created
 *   - with {@link FlowLogger#executionLogger} as prototype if `getFlowIdentification` is provided
 *   - with {@link FlowLogger#current} as prototype if `getFlowIdentification` is not provided
 * - {@link FlowLogger#current} is set to the new {@link FlowLogger}
 * - the call to the function is logged with {@link Phase#CALLED} on the new {@link FlowLogger}, with the provided
 *   `arguments`
 * - the function is called, with the provided `arguments`
 *
 * Call with `getFlowIdentification` to decorate functions where a flow enters the execution. Call without
 * `getFlowIdentification` to decorate functions that are part of an existing flow.
 *
 * When the function call ends
 * - {@link FlowLogger#current} is (re)set to the prototype of the new {@link FlowLogger}
 * - if the function returns
 *   - if the ending is {@link Promise}-like, the return is logged with {@link Phase#WAITING}
 *   - if the ending is not {@link Promise}-like, the result and resulting value is logged with
 *     {@link Phase#RETURNS}
 * - if the function throws, the ending is logged with {@link Phase#THROWS}
 * - if a {@link Promise}-like result was returned, when it settles,
 *   - if the {@link Promise}-like result resolves, the resolution is logged with {@link Phase#RETURNS}
 *   - if the {@link Promise}-like result rejects, the rejection is logged with {@link Phase#THROWS}
 *
 * During execution of the function, anywhere in the body, {@link FlowLogger#current} refers to the new
 * {@link FlowLogger}, because
 * - it is set that way at the start
 * - it is reset when any logging function call in the body ends
 * - it is reset when any async logging function call in the body settles
 *
 * // MUDO find a way to determine when or how to log {@link Phase#FATAL}
 * // MUDO at this time, everyting is logged at level INFO; do something about that
 *
 * @param {function} f - the function to add the logging aspect too
 * @param {function} [getFlowIdentification] - Optional function. If provided, is called with the `this` and arguments
 *                                             the subject function is called with, and should return the
 *                                             {@link FlowIdentification} of the new flow to log for. The idea is that
 *                                             this function extracts the {@link FlowIdentification} from the arguments
 *                                             of the function where a flow enters the execution. Must be an
 *                                             implementation that adheres to {@link getFlowIdentificationContract}.
 * @return {function} - Proxy of `f` that logs
 */
const logFunctionProxy = new Contract({
  pre: [
    f => typeof f === 'function',
    (f, getFlowIdentification) =>
      getFlowIdentification === undefined || getFlowIdentificationContract.isImplementedBy(getFlowIdentification)
  ],
  post: [
    function (f) {
      return Contract.outcome(arguments) !== f
    },
    function (f) {
      return Contract.outcome(arguments).name === f.name
    },
    // cannot express that result[[[Target]]] === f, because Symbol [[Target]] is private
    function (f) {
      return (
        !f.prototype ||
        typeof f.prototype !== 'object' ||
        !Object.prototype.hasOwnProperty.call(f.prototype, 'constructor') ||
        (Object.prototype.hasOwnProperty.call(Contract.outcome(arguments).prototype, 'constructor') &&
          Contract.outcome(arguments).prototype.constructor === Contract.outcome(arguments))
      )
    }
  ]
}).implementation(function logFunctionProxy (f, getFlowIdentification) {
  if (getFlowIdentification) {
    // because this is a hook method
    getFlowIdentification.contract.verifyPostconditions = true
  }
  let prototypeConstructorDescriptor
  if (!!f.prototype && typeof f.prototype === 'object') {
    prototypeConstructorDescriptor = Object.getOwnPropertyDescriptor(f.prototype, 'constructor')
  }
  const result = new Proxy(f, {
    construct: construct.bind(undefined, getFlowIdentification),
    apply: apply.bind(undefined, getFlowIdentification)
  })
  if (prototypeConstructorDescriptor) {
    prototypeConstructorDescriptor.value = result
    Object.defineProperty(f.prototype, 'constructor', prototypeConstructorDescriptor)
  }
  return result
})

module.exports = { createLogger, getFlowIdentificationContract, construct, apply, logFunctionProxy }
