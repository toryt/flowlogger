/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
/** @type {ContractFunction} */ const SyncWriter = require('./SyncWriter')
const Level = require('../Level')

/**
 * Writer that writes all messages immediately to the console, interpreted, on multiple lines.
 * Can be used during development.
 *
 * All messages {@link #post}ed are written immediately. Instances are always {@link #isFlushed}, and {@link #flush}
 * returns with a resolved {@link Promise}.
 */
const SyncConsoleWriterContract = new Contract({
  post: [
    function () {
      return this.isFlushed()
    }
  ]
})

SyncConsoleWriterContract.extends = SyncWriter.contract

SyncConsoleWriterContract.methods = Object.create(SyncConsoleWriterContract.extends.methods)

SyncConsoleWriterContract.invariants = SyncConsoleWriterContract.extends.invariants

function SyncConsoleWriterImpl () {
  SyncWriter.call(this)
}

SyncConsoleWriterImpl.prototype = Object.create(SyncWriter.prototype)
SyncConsoleWriterImpl.prototype.constructor = SyncConsoleWriterImpl

SyncConsoleWriterImpl.prototype.post = SyncConsoleWriterContract.methods.post.implementation(function post (message) {
  const logMethodName =
    // prettier-ignore
    message.level === Level.ERROR || message.level === Level.FATAL
      ? 'error'
      : message.level === Level.WARN
        ? 'warn'
        : message.level === Level.INFO
          ? 'info'
          : 'debug' // message.level === Level.TRACE || message.level === Level.DEBUG and other

  function write (str) {
    console[logMethodName](str)
  }
  console.group(
    `${message.level.toString()} — ${message.phase.toString()} - ${message.function} - ${message.flow} — ${
      message.execution.pid
    }`
  )
  console.groupCollapsed('details')
  write(`𝕋流 v${message['𝕋流'].version} — log structure: ${message['𝕋流'].version}`)
  write(`${message.program.name} — v${message.program.version}`)
  write(`${message.execution.hostname} — ${message.execution.pid}`)
  console.groupEnd()
  console.dir(message.data)
  console.groupEnd()
  console.log() // empty line
})

/* implementation specific invariants */
SyncConsoleWriterImpl.prototype.invariants = SyncConsoleWriterContract.invariants.slice()

module.exports = SyncConsoleWriterContract.implementation(SyncConsoleWriterImpl)
