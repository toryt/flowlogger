/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
/** @type {ContractFunction} */ const SyncWriter = require('./SyncWriter')
const validateSchema = require('../_util/validateSchema')
const WriterMessage = require('../WriterMessage')

/**
 * Writer that remembers all {@link #post}s in memory. Can be used to test logging in (which you probably only should do
 * in extreme situations).
 *
 * All messages {@link #post}ed are stored, in order, in the array {@link #messages}. Instances are always
 * {@link #isFlushed}, and {@link #flush} returns with a resolved {@link Promise}.
 */
const SyncMemWriterContract = new Contract({
  post: [
    function () {
      return this.isFlushed()
    }
  ]
})

SyncMemWriterContract.extends = SyncWriter.contract

SyncMemWriterContract.methods = Object.create(SyncMemWriterContract.extends.methods)

SyncMemWriterContract.invariants = SyncMemWriterContract.extends.invariants.concat([
  function () {
    return Array.isArray(this.messages)
  },
  function () {
    return this.messages.every(m => validateSchema(WriterMessage.schema, m))
  }
])

function SyncMemWriterImpl () {
  SyncWriter.call(this)
  this.messages = []
}

SyncMemWriterImpl.prototype = Object.create(SyncWriter.prototype)
SyncMemWriterImpl.prototype.constructor = SyncMemWriterImpl

SyncMemWriterImpl.prototype.messages = undefined

SyncMemWriterImpl.prototype.post = SyncMemWriterContract.methods.post.implementation(function post (message) {
  this.messages.push(message)
})

/* implementation specific invariants */
SyncMemWriterImpl.prototype.invariants = SyncMemWriterContract.invariants.slice()

module.exports = SyncMemWriterContract.implementation(SyncMemWriterImpl)
