/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
const PromiseContract = require('@toryt/contracts-iv').Promise
const conditions = require('../_util/conditions')
/** @type {ContractFunction} */ const Writer = require('../Writer')

/**
 * Writer that repeats {@link #flushOnce} until all messages are written. When {@link #flush} is called again while
 * a previous call is still pending, the same {@link Promise} is returned.
 */
const AsyncWriterContract = new Contract({
  post: [
    function () {
      return this.isFlushed()
    }
  ]
})

AsyncWriterContract.extends = Writer.contract
AsyncWriterContract.methods = Object.create(AsyncWriterContract.extends.methods)

/**
 * Flush the messages that are known when called. Will be called again if there are new messages when done.
 *
 * `flushOnce` is a protected method. It does not uphold the invariant `!(this._pendingFlush && this.isFlushed())`.
 * It does not bother with `this._pendingFlush`.
 *
 * @param {WriterMessage} message
 */
AsyncWriterContract.methods.flushOnce = new PromiseContract({
  post: [conditions.isVoid]
})

AsyncWriterContract.invariants = AsyncWriterContract.extends.invariants.concat([
  function () {
    return AsyncWriterContract.methods.flushOnce.isImplementedBy(this.flushOnce)
  }
])

function AsyncWriterImpl () {
  Writer.call(this)
}

AsyncWriterImpl.prototype = Object.create(Writer.prototype)
AsyncWriterImpl.prototype.constructor = AsyncWriterImpl

AsyncWriterImpl.prototype._pendingFlush = undefined

AsyncWriterImpl.prototype.flushOnce = AsyncWriterContract.methods.flushOnce.abstract

// TODO add contract and test separately
AsyncWriterImpl.prototype._repeatFlushAndForget = async function _repeatFlushAndForget () {
  while (!this.isFlushed()) {
    await this.flushOnce()
    // by now, messages could have been added; check again
  }
  this._pendingFlush = undefined
}

const resolved = Promise.resolve()

AsyncWriterImpl.prototype.flush = AsyncWriterContract.methods.flush.implementation(function flush () {
  if (this.isFlushed()) {
    return resolved
  }
  if (!this._pendingFlush) {
    this._pendingFlush = this._repeatFlushAndForget()
  }
  return this._pendingFlush
})

/* implementation specific invariants */
AsyncWriterImpl.prototype.invariants = AsyncWriterContract.invariants.concat([
  function () {
    return !this._pendingFlush || this._pendingFlush instanceof Promise
  },
  function () {
    return !(this._pendingFlush && this.isFlushed())
  }
])

module.exports = AsyncWriterContract.implementation(AsyncWriterImpl)
