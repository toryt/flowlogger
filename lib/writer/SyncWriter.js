/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
/** @type {ContractFunction} */ const Writer = require('../Writer')

/**
 * Writer that writes structured log records immediately.
 *
 * Instances are always {@link #isFlushed}, and {@link #flush} returns with a resolved {@link Promise}.
 */
const SyncWriterContract = new Contract({
  post: [
    function () {
      return this.isFlushed()
    }
  ]
})

SyncWriterContract.extends = Writer.contract

SyncWriterContract.methods = Object.create(SyncWriterContract.extends.methods)

SyncWriterContract.invariants = SyncWriterContract.extends.invariants.concat([
  function () {
    return this.isFlushed()
  }
])

function SyncWriterImpl () {
  Writer.call(this)
}

SyncWriterImpl.prototype = Object.create(Writer.prototype)
SyncWriterImpl.prototype.constructor = SyncWriterImpl

SyncWriterImpl.prototype.isFlushed = SyncWriterContract.methods.isFlushed.implementation(function isFlushed () {
  return true
})

const resolved = Promise.resolve()
SyncWriterImpl.prototype.flush = SyncWriterContract.methods.flush.implementation(function flush () {
  return resolved
})

/* implementation specific invariants */
SyncWriterImpl.prototype.invariants = SyncWriterContract.invariants.slice()

module.exports = SyncWriterContract.implementation(SyncWriterImpl)
