/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
const PromiseContract = require('@toryt/contracts-iv').Promise
/** @type {ContractFunction} */ const AsyncWriter = require('./AsyncWriter')
const validateSchema = require('../_util/validateSchema')
const WriterMessage = require('../WriterMessage')
const Joi = require('@hapi/joi')
const path = require('path')
const conditions = require('../_util/conditions')
const fs = require('fs-extra')

/**
 * Writer that remembers messages until flush, and writes JSON representation of structured log records to a file
 * per flow, asynchronously, on {@link flush}. Intended for usage during automated tests of your business project.
 *
 * {@link #post} stores the message. {@link #flush} creates subdirectories in the `logDirectoryPath` per day and
 * execution, and a JSON file per flow. The JSON file contains an array of the stored messages for the flow,
 * in the order received.
 *
 * @param {string} logDirectoryPath - path to the log directory; relative to the project when a relative path is
 *                                    specified
 */
const AsyncDelayedFileWriterContract = new Contract({
  pre: [logDirectoryPath => !!logDirectoryPath, logDirectoryPath => typeof logDirectoryPath === 'string'],
  post: [
    function (logDirectoryPath) {
      return this.logDirectoryPath === logDirectoryPath
    },
    function () {
      return this.isFlushed()
    }
  ]
})

AsyncDelayedFileWriterContract.extends = AsyncWriter.contract

AsyncDelayedFileWriterContract.methods = Object.create(AsyncDelayedFileWriterContract.extends.methods)

/* IDEA: It would be a good idea to extend the flush contract here, with explicit postconditions about what
         AsyncDelayedFileWriter does, but that requires recording old. That is not supported in Contracts yet, and the known
         workaround is too dangerous for this library. This is tested in the tests. */

AsyncDelayedFileWriterContract.invariants = AsyncDelayedFileWriterContract.extends.invariants // no extra invariants

function AsyncDelayedFileWriterImpl (logDirectoryPath) {
  AsyncWriter.call(this)
  this.logDirectoryPath = logDirectoryPath
  this._messages = {}
}

AsyncDelayedFileWriterImpl.prototype = Object.create(AsyncWriter.prototype)
AsyncDelayedFileWriterImpl.prototype.constructor = AsyncDelayedFileWriterImpl

AsyncDelayedFileWriterImpl.prototype.logDirectoryPath = undefined
AsyncDelayedFileWriterImpl.prototype._messages = undefined

const PostImplContract = new PromiseContract({
  pre: AsyncDelayedFileWriterContract.methods.post.pre.slice(),
  post: AsyncDelayedFileWriterContract.methods.post.post.concat([
    function (message) {
      return !!this._messages[message.at]
    },
    function (message) {
      return !!this._messages[message.at][message.execution.id]
    },
    function (message) {
      return !!this._messages[message.at][message.execution.id][message.flow]
    },
    function (message) {
      return this._messages[message.at][message.execution.id][message.flow].includes(message)
    }
  ])
})

// build contract prototype chain, while waiting for an extends in Contracts
Object.setPrototypeOf(PostImplContract, AsyncDelayedFileWriterContract.methods.post)

AsyncDelayedFileWriterImpl.prototype.post = PostImplContract.implementation(function post (message) {
  if (!this._messages[message.at]) {
    this._messages[message.at] = {}
  }
  if (!this._messages[message.at][message.execution.id]) {
    this._messages[message.at][message.execution.id] = {}
  }
  if (!this._messages[message.at][message.execution.id][message.flow]) {
    this._messages[message.at][message.execution.id][message.flow] = []
  }
  this._messages[message.at][message.execution.id][message.flow].push(message)
})

AsyncDelayedFileWriterImpl.prototype.isFlushed = AsyncDelayedFileWriterContract.methods.isFlushed.implementation(
  function isFlushed () {
    return Object.keys(this._messages).length === 0
  }
)

const messagesSchema = Joi.array()
  .items(WriterMessage.schema.required())
  .required()

/**
 * Return `messages` if there is no JSON file at `filePath`, and the contents of `filePath`, with `messages` appended if
 * there is.
 *
 * @param {Array<WriterMessage>} messages
 * @param {string} filePath
 */
AsyncDelayedFileWriterImpl.prototype._messagesToWrite = new PromiseContract({
  pre: [
    messages => validateSchema(messagesSchema, messages),
    (messages, filePath) => !!filePath,
    (messages, filePath) => typeof filePath === 'string',
    (messages, filePath) => path.parse(filePath).ext === '.json'
  ],
  post: [
    (messages, filePath, result) => validateSchema(messagesSchema, result),
    (messages, filePath, result) => messages.every((m, i) => m === result[result.length - messages.length + i]),
    async (messages, filePath, result) => {
      if (await fs.pathExists(filePath)) {
        const older = await fs.readJSON(filePath)
        return result.length === older.length + messages.length && result.every((m, i) => m === result[i])
      }
      return result.length === messages.length
    }
  ]
}).implementation(async function _writeFlow (messages, filePath) {
  try {
    const existing = messagesSchema.validate(await fs.readJSON(filePath)).value
    return existing.concat(messages)
  } catch (err) {
    if (err.code !== 'ENOENT') {
      throw err
    }
    return messages
  }
})

/**
 * Do a best effort to write `this._messages[dateString][executionId][flowId]` as a JSON array of {@link WriterMessage}s
 * to the file `{$flowId}.json` in the directory `${this.logDirectoryPath}/${dateString}/${executionId}`,
 * or append the elements of `this._messages[dateString][executionId][flowId]` to the array in that file if it already
 * exists.
 *
 * If stringification or writing fails, the error is reported on the console, but no exception or error is thrown.
 *
 * @param {string} dateString
 * @param {string} executionId
 * @param {string} flowId
 */
AsyncDelayedFileWriterImpl.prototype._writeFlow = new PromiseContract({
  pre: [
    dateString => validateSchema(isoDateString, dateString),
    function (dateString) {
      return !!this._messages[dateString]
    },
    (dateString, executionId) => validateSchema(uuidv4String, executionId),
    function (dateString, executionId) {
      return !!this._messages[dateString][executionId]
    },
    (dateString, executionId, flowId) => validateSchema(uuidv4String, flowId),
    function (dateString, executionId, flowId) {
      return !!this._messages[dateString][executionId][flowId]
    }
  ],
  post: [
    conditions.isVoid,
    function (dateString, executionId, flowId) {
      return (
        !this._messages[dateString] || !this._messages[dateString][executionId] || !this._messages[dateString][flowId]
      )
    }
  ]
}).implementation(async function _writeFlow (dateString, executionId, flowId) {
  const filePath = path.format({
    dir: path.join(this.logDirectoryPath, dateString, executionId),
    name: flowId,
    ext: '.json'
  })
  try {
    const messages = await this._messagesToWrite(this._messages[dateString][executionId][flowId].slice(), filePath)
    await fs.outputJSON(filePath, messages, { spaces: 2 })
  } catch (err) {
    console.error(`Could not write flow ${filePath}`, err)
  } finally {
    delete this._messages[dateString][executionId][flowId]
  }
})

/**
 * Do a best effort to write `this._messages[dateString][executionId]` with {@link #_writeFlow}
 * in the director `${this.logDirectoryPath}/${dateString}/${executionId}`.
 *
 * @param {string} dateString
 * @param {string} executionId
 */
AsyncDelayedFileWriterImpl.prototype._writeExecution = new PromiseContract({
  pre: [
    dateString => validateSchema(isoDateString, dateString),
    function (dateString) {
      return !!this._messages[dateString]
    },
    (dateString, executionId) => validateSchema(uuidv4String, executionId),
    function (dateString, executionId) {
      return !!this._messages[dateString][executionId]
    }
  ],
  post: [
    conditions.isVoid,
    function (dateString, executionId) {
      return !this._messages[dateString][executionId]
    }
  ]
}).implementation(async function _writeExecution (dateString, executionId) {
  await Promise.all(
    Object.keys(this._messages[dateString][executionId]).map(flowId => this._writeFlow(dateString, executionId, flowId))
  )
  delete this._messages[dateString][executionId]
})

/**
 * Do a best effort to write `this._messages[dateString]` with {@link #_writeExecution}
 * in the directory `${this.logDirectoryPath}/${dateString}`.
 *
 * @param {string} dateString
 */
AsyncDelayedFileWriterImpl.prototype._writeDay = new PromiseContract({
  pre: [
    dateString => validateSchema(isoDateString, dateString),
    function (dateString) {
      return !!this._messages[dateString]
    }
  ],
  post: [
    conditions.isVoid,
    function (dateString) {
      return !this._messages[dateString]
    }
  ]
}).implementation(async function _writeDay (dateString) {
  await Promise.all(
    Object.keys(this._messages[dateString]).map(executionId => this._writeExecution(dateString, executionId))
  )
  delete this._messages[dateString]
})

AsyncDelayedFileWriterImpl.prototype.flushOnce = AsyncDelayedFileWriterContract.methods.flushOnce.implementation(
  async function flushOnce () {
    await Promise.all(Object.keys(this._messages).map(dateString => this._writeDay(dateString)))
  }
)

const flowMessages = Joi.array().items(WriterMessage.schema.required())

const uuidv4String = Joi.string().uuid({
  version: ['uuidv4']
})

const executionMessages = Joi.object().pattern(uuidv4String.label('flow'), flowMessages)

const dateMessages = Joi.object().pattern(uuidv4String.label('execution'), executionMessages)

const isoDateString = Joi.string()
  .isoDate()
  .label('date')

const messages = Joi.object()
  .pattern(isoDateString, dateMessages)
  .required()

/* implementation specific invariants */
AsyncDelayedFileWriterImpl.prototype.invariants = AsyncDelayedFileWriterContract.invariants.concat([
  function () {
    return validateSchema(messages, this._messages)
  },
  function () {
    return this.isFlushed() === (Object.entries(this._messages).length === 0)
  }
])

module.exports = AsyncDelayedFileWriterContract.implementation(AsyncDelayedFileWriterImpl)
