/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
const PromiseContract = require('@toryt/contracts-iv').Promise
const conditions = require('../_util/conditions')
/** @type {ContractFunction} */ const AsyncWriter = require('./AsyncWriter')
const validateSchema = require('../_util/validateSchema')
const WriterMessage = require('../WriterMessage')

/**
 * Writer that writes JSON representation asynchronously, eventually using {@link #write}.
 *
 * {@link #write} is called on the next tick after {@link #post} is called. {@link #post} stores a {@link Promise}.
 * {@link #flush} waits until all stored {@link Promise}s are resolved or rejected. {@link #isFlushed} returns `true`
 * when there are no pending stored {@link Promise}s.
 *
 * Any {@link #write} rejections are discarded. Logging is 'best effort'.
 */
const AsyncImmediateWriterContract = new Contract({
  post: [
    function () {
      return this.isFlushed()
    }
  ]
})

AsyncImmediateWriterContract.extends = AsyncWriter.contract

AsyncImmediateWriterContract.methods = Object.create(AsyncImmediateWriterContract.extends.methods)

/**
 * Write `message` to log storage, and resolve when done (or reject when failed).
 * Should not fail, but the framework deals with it if it does.
 *
 * @param {WriterMessage} message
 */
AsyncImmediateWriterContract.methods.write = new PromiseContract({
  pre: [validateSchema.bind(null, WriterMessage.schema.required())],
  post: [conditions.isVoid],
  exception: [() => true]
})

AsyncImmediateWriterContract.invariants = AsyncImmediateWriterContract.extends.invariants.concat([
  function () {
    return AsyncImmediateWriterContract.methods.write.isImplementedBy(this.write)
  }
])

function AsyncImmediateWriterImpl () {
  AsyncWriter.call(this)
  this._promises = new Set()
}

AsyncImmediateWriterImpl.prototype = Object.create(AsyncWriter.prototype)
AsyncImmediateWriterImpl.prototype.constructor = AsyncImmediateWriterImpl

AsyncImmediateWriterImpl.prototype._promises = undefined

AsyncImmediateWriterImpl.prototype.write = AsyncImmediateWriterContract.methods.write.abstract

AsyncImmediateWriterImpl.prototype.post = AsyncImmediateWriterContract.methods.post.implementation(function post (
  message
) {
  const promise = new Promise(resolve => {
    setTimeout(async () => {
      try {
        await this.write(message)
      } catch (err) {
        console.error('Could not write message', err)
        // MUDO better output? JSON?
      } finally {
        this._promises.delete(promise)
        resolve()
      }
    }, 0)
  })
  this._promises.add(promise)
})

AsyncImmediateWriterImpl.prototype.isFlushed = AsyncImmediateWriterContract.methods.isFlushed.implementation(
  function isFlushed () {
    return this._promises.size <= 0
  }
)

AsyncImmediateWriterImpl.prototype.flushOnce = AsyncImmediateWriterContract.methods.flushOnce.implementation(
  async function flushOnce () {
    await Promise.allSettled(Array.from(this._promises))
  }
)

/* implementation specific invariants */
AsyncImmediateWriterImpl.prototype.invariants = AsyncImmediateWriterContract.invariants.concat([
  function () {
    return this._promises instanceof Set
  },
  function () {
    return Array.from(this._promises.values()).every(p => p instanceof Promise)
  }
])

module.exports = AsyncImmediateWriterContract.implementation(AsyncImmediateWriterImpl)
