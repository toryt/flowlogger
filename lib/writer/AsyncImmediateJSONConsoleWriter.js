/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
/** @type {ContractFunction} */ const AsyncImmediateWriter = require('./AsyncImmediateWriter')

/**
 * Writer that writes JSON representation to console asynchronously, always using {@link console#log}, whatever the
 * level.
 */
const AsyncImmediateJSONConsoleWriterContract = new Contract({})

AsyncImmediateJSONConsoleWriterContract.extends = AsyncImmediateWriter.contract

AsyncImmediateJSONConsoleWriterContract.methods = Object.create(AsyncImmediateJSONConsoleWriterContract.extends.methods)

AsyncImmediateJSONConsoleWriterContract.invariants = AsyncImmediateJSONConsoleWriterContract.extends.invariants // no extra invariants

function AsyncImmediateJSONConsoleWriterImpl () {
  AsyncImmediateWriter.call(this)
}

AsyncImmediateJSONConsoleWriterImpl.prototype = Object.create(AsyncImmediateWriter.prototype)
AsyncImmediateJSONConsoleWriterImpl.prototype.constructor = AsyncImmediateJSONConsoleWriterImpl

AsyncImmediateJSONConsoleWriterImpl.prototype.write = AsyncImmediateJSONConsoleWriterContract.methods.write.implementation(
  async function write (message) {
    console.log(JSON.stringify(message))
  }
)

/* implementation specific invariants */
AsyncImmediateJSONConsoleWriterImpl.prototype.invariants = AsyncImmediateJSONConsoleWriterContract.invariants.slice()

module.exports = AsyncImmediateJSONConsoleWriterContract.implementation(AsyncImmediateJSONConsoleWriterImpl)
