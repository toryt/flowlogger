/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @type Joi */ const Joi = require('@hapi/joi')

/**
 * @type {HrTime}
 */
const example1 = '2020-01-28T04:28:48.797167311Z'
const example2 = '2020-01-28T08:08:30.194057Z'
const example3 = '2020-01-28T08:08:30Z'
const example4 = '1970-01-01T00:00:00.000000000Z'

const hrTimePattern = /^\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])T(?:[01]\d|2[0-3])(?::(?:[0-5]\d)){2}(\.\d{0,9})?Z$/

/**
 * High-resolution time. Expresses a time in UTC (`Z`), with ns precision, as ISO-8601 string.
 *
 * This can be used as parameter of `new Date(iso)`, to produce a correct instance with ms (millisecond) precision.
 *
 * The pattern is more strict than general ISO-8601.
 *
 * Note: to get high-resolution time in Node, you call `process.hrtime`. This returns the time passed as s and ms since
 * an arbitrary point-in-time, _not_ Epoch. Libraries like `nano-second` work around this. This also means that the
 * high-resolution time is not exactly correct to atomic clocks (which would be non-sensical at that precision,
 * given machine's clock drift, or special relativity in general anyway). Rather, the libraries make sure that the
 * high-resolution time is exact to the machine's clock with ms precision, but with the same time difference for every
 * call. _reported time - "real time"_ will be the same for every call, so that the time difference
 * _reported time 2 - reported time 1_ will be correct with ns precision when when both times are reported in the same
 * process on the same machine (the sub-ms difference with "real time" cancels out).
 *
 * Note: in recent versions of Node, this is expressed as {@link BigInt}. The format
 * `[seconds-since-epoch, nano-seconds]` is a legacy format. That is irrelevant, since this library only works with
 * a string ISO-representation of time.
 *
 * @typedef HrTime
 * @type {string}
 */

/** {@link StringSchema} */
const hrTime = Joi.string()
  .pattern(hrTimePattern)
  .example(example1)
  .example(example2)
  .example(example3)
  .example(example4)

module.exports = { schema: hrTime, example: example1 }
