/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const version = require('../package').version
const semver = require('semver')

/**
 * The semantic version of the structure of JSON records emitted by flowlogger.
 * This should not be changed by usage code.
 *
 * @type {string}
 */
const logStructureVersion = '1.0.0'

/**
 * Identifies the `flowlogger 𝕋流` version and structured log record structure.
 *
 * @typedef FlowloggerIdentification
 * @type {object}
 * @property {string} version - version of flowlogger that emitted the log record
 * @property {string} logStructureVersion - semantic version of the structure of JSON records emitted by flowlogger
 */

/** @type {FlowloggerIdentification} */ const flowloggerIdentification = {
  version: semver.valid(version),
  logStructureVersion: logStructureVersion
}

module.exports = flowloggerIdentification
