/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @type Joi */ const Joi = require('@hapi/joi')

/**
 * @type {FlowIdentification}
 */
const example = '34703125-816b-4c2d-aaf5-a7679ac839c0'

/**
 * UUID that uniquely identifies a particular flow
 *
 * @typedef FlowIdentification
 * @type {string}
 */

/** @type {StringSchema} */
const flowIdentification = Joi.string()
  .uuid({
    version: ['uuidv4']
  })
  .example(example)

module.exports = { schema: flowIdentification, example }
