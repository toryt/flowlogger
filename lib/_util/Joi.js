/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const BaseJoi = require('@hapi/joi')
const semver = require('semver')

const extendedJoi = BaseJoi.extend(joi => ({
  type: 'semver',
  base: joi.string(),
  messages: {
    semver: '{{#label}} must be a valid semantic version'
  },
  validate: (value, helpers) => {
    const coerced = semver.valid(value)
    if (!coerced) {
      return { value, errors: helpers.error('semver') }
    }
  }
}))

module.exports = extendedJoi
