/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @param {Schema|StringSchema|ObjectSchema} schema
 * @param {*} value
 * @param {object} [context]
 */
function validateSchema (schema, value, context) {
  const error = schema.validate(value, { convert: false, context }).error
  if (!error) {
    return true
  }
  console.error(error.annotate())
  return false
}

module.exports = validateSchema
