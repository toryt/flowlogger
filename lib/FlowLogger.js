/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
const PromiseContract = require('@toryt/contracts-iv').Promise
const { resultIsArg, isVoid } = require('./_util/conditions')
const Level = require('./Level')
const Phase = require('./Phase')
const prepare = require('./prepare')
const os = require('os')
const semver = require('semver')
const { v4: uuidv4 } = require('uuid')
const Joi = require('@hapi/joi')
const validateSchema = require('./_util/validateSchema')
const flowLoggerIdentification = require('./flowloggerIdentification')
const ProgramIdentification = require('./ProgramIdentification')
const ExecutionIdentification = require('./ExecutionIdentification')
const FlowIdentification = require('./FlowIdentification')
const Writer = require('./Writer')
const nano = require('nano-seconds')

const Base = Joi.object().keys({
  '𝕋流': Joi.object()
    .valid(flowLoggerIdentification)
    .required(),
  program: ProgramIdentification.schema.required(),
  execution: ExecutionIdentification.schema.required(),
  flow: FlowIdentification.schema.optional(), // required in subclasses
  function: Joi.string().optional(), // required in subclasses
  writer: Joi.object().instance(Writer)
})

function thisIsCurrent () {
  return this === FlowLogger.current
}

/**
 * Create an instance that holds the base data to output in all structured log records in an execution.
 *
 * @constructor
 * @property {Contract} contract
 * @param {string} kwargs.programName - name of the program that uses `flowlogger 𝕋流`
 * @param {string} kwargs.programVersion - semantic version of the program that uses `flowlogger 𝕋流`
 * @param {string} kwargs.dataStructureVersion - semantic version of the structure of the values of the `data` property
 *                                               of the structured records that will be logged
 * @param {Writer} kwargs.writer - instance that will output structured log records
 */
const FlowLogger = new Contract({
  pre: [
    kwargs => !!kwargs,
    kwargs => typeof kwargs === 'object',
    kwargs => !!kwargs.programName,
    kwargs => typeof kwargs.programName === 'string',
    kwargs => semver.valid(kwargs.programVersion),
    kwargs => semver.valid(kwargs.logDataVersion),
    kwargs => kwargs.writer instanceof Writer
  ],
  post: [
    function (kwargs) {
      return this.program.name === kwargs.programName
    },
    function (kwargs) {
      return this.program.version === semver.valid(kwargs.programVersion)
    },
    function (kwargs) {
      return this.program.logDataVersion === semver.valid(kwargs.logDataVersion)
    },
    function () {
      return this.execution.hostname === os.hostname()
    },
    function () {
      return this.execution.pid === process.pid
    },
    function (kwargs) {
      return this.writer === kwargs.writer
    }
  ]
}).implementation(function FlowLogger (kwargs) {
  this.program = {
    name: kwargs.programName,
    version: semver.valid(kwargs.programVersion),
    logDataVersion: semver.valid(kwargs.logDataVersion)
  }
  this.execution = { id: uuidv4(), hostname: os.hostname(), pid: process.pid }
  this.writer = kwargs.writer
})

FlowLogger.prototype.constructor = FlowLogger

/**
 * Information about the version of flowlogger used. This is part of the distribution, and should not be changed by
 * usage code.
 */
FlowLogger.prototype.𝕋流 = flowLoggerIdentification

/**
 * Information about the program that uses `flowlogger 𝕋流`
 *
 * @type {ProgramIdentification}
 */
FlowLogger.prototype.program = undefined

/**
 * Information about the program that uses `flowlogger 𝕋流`
 *
 * @type {ExecutionIdentification}
 */
FlowLogger.prototype.execution = undefined

/**
 * {@link Writer} that will output structured log records
 *
 * @type {Writer}
 */
FlowLogger.prototype.writer = undefined

FlowLogger.prototype.invariants = [
  function () {
    return validateSchema(Base, this)
  },
  function () {
    return validateSchema(FlowIdentification.schema, this.flow) // not required
  },
  function () {
    return !this.function || typeof this.function === 'string' // not required
  },
  function () {
    return this.writer instanceof Writer
  }
]

FlowLogger.prototype.log = new Contract({
  pre: [
    level => validateSchema(Level.schema, level),
    (level, phase) => validateSchema(Phase.schema, phase),
    (level, phase, rawData) => typeof rawData === 'object',
    (level, phase, rawData) => rawData !== null
  ],
  post: [isVoid]
}).implementation(function log (level, phase, rawData) {
  this.writer.post({
    level,
    at: nano.toISOString(),
    𝕋流: this.𝕋流,
    program: this.program,
    execution: this.execution,
    flow: this.flow,
    function: this.function,
    phase,
    data: prepare(rawData)
  })
})

FlowLogger.prototype.initialized = new Contract({ post: [isVoid] }).implementation(function initialized () {
  this.log(Level.INFO, Phase.INITIALIZED, {
    executionDetails: {
      machine: {
        hostname: os.hostname(),
        ipAddresses: Object.values(os.networkInterfaces()).reduce(
          (acc, nis) =>
            nis.reduce((acc, ni) => {
              if (ni.internal) {
                return acc
              }
              acc[ni.family.toLowerCase()].push(ni.address)
              return acc
            }, acc),
          { ipv4: [], ipv6: [] }
        ),
        arch: os.arch(),
        '#cpu': os.cpus().length,
        mem: os.totalmem()
      },
      os: {
        type: os.type(),
        name: os.platform(),
        version: os.release()
      },
      engine: {
        name: process.release.name,
        version: process.versions.node,
        lts: process.release.lts
      },
      process: {
        pid: process.pid,
        uid: process.getuid(),
        gid: process.getgid(),
        cwd: process.cwd(),
        env: process.env,
        argv: process.argv
      }
    }
  })
  FlowLogger.executionLogger = this
})

/**
 * Record the call of a logging function.
 *
 * @param {arguments} args - the arguments with wich the logging function is called
 */
FlowLogger.prototype.called = new Contract({
  pre: [
    function () {
      return !!this.flow
    }
  ],
  post: [isVoid]
}).implementation(function called (args) {
  this.log(Level.INFO, Phase.CALLED, { arguments: args })
})

// MUDO should return a new logger, and not log itelf? Not necessary for selection?
FlowLogger.prototype.selection = new Contract({
  pre: [
    function () {
      return !!this.flow
    },
    thisIsCurrent
  ],
  post: [isVoid]
}).implementation(function selection (selectionName) {
  this.log(Level.INFO, Phase.SELECTION, { selectionName })
})

// MUDO should return a new logger, and not log itelf?
FlowLogger.prototype.iteration = new Contract({
  pre: [
    function () {
      return !!this.flow
    },
    thisIsCurrent
  ],
  post: [isVoid]
}).implementation(function iteration (data) {
  this.log(Level.TRACE, Phase.ITERATION, { data })
})

/**
 * Record the return of a {@link Promise} by a logging function, and return it.
 *
 * @param {any} promise - promise to be reported on
 * @returns {any} `promise`
 */
FlowLogger.prototype.waiting = new Contract({
  pre: [
    function () {
      return !!this.flow
    },
    function (promise) {
      return !!promise && typeof promise.then === 'function' && typeof promise.catch === 'function'
    }
  ],
  post: [resultIsArg]
}).implementation(function waiting (promise) {
  this.log(Level.INFO, Phase.WAITING, {}) // MUDO empty raw data
  return promise
})

/**
 * Record the return of a logging function, and return the `result`.
 *
 * @param {any} result - the result to be recorded; can be anything
 * @returns {any} `result`
 */
FlowLogger.prototype.returns = new Contract({
  pre: [
    function () {
      return !!this.flow
    }
  ],
  post: [resultIsArg]
}).implementation(function returns (result) {
  this.log(Level.INFO, Phase.RETURNS, { result })
  return result
})

/**
 * Record the throwing of a logging function, and return the `thrown`.
 *
 * @param {any} thrown - the thrown to be recorded; can be anything, but should be an {@link Error}
 * @returns {any} `thrown`
 */
FlowLogger.prototype.throws = new Contract({
  pre: [
    function () {
      return !!this.flow
    }
  ],
  post: [resultIsArg]
}).implementation(function throws (thrown) {
  this.log(Level.INFO, Phase.THROWS, { thrown })
  return thrown
})

/**
 * Record an unexpected, non-fatal, error ending of the subject function, and return the `error`.
 *
 * Do not introduce a code branch merely to be able to log this error. That branch would be impossible or very
 * difficult to test, is unnecessary, but might still contain bugs like any other code. There is no functional
 * reason to have this branch, so do not write it. This method should only be used when you have to throw
 * the error yourself as the outcome of the subject function.
 *
 * @example
 * function f() {
 *   const 📝 = flowlogger(arguments)
 *   …
 *   throw 📝.error(new Error(…))
 *   …
 * }
 * @param {any} err - the error to be recorded; can be anything, but should be an {@link Error}
 * @returns {any} `err`
 */
FlowLogger.prototype.error = new Contract({
  pre: [
    function () {
      return !!this.flow
    }
  ],
  post: [resultIsArg]
}).implementation(function error (err) {
  this.log(Level.ERROR, Phase.THROWS, { err })
  return err // MUDO change to throw?
})

/**
 * Record an unexpected, fatal error ending of the subject function and the process, and return the `error`.
 *
 * Do not introduce a code branch merely to be able to log this error. That branch would be impossible or very
 * difficult to test, is unnecessary, but might still contain bugs like any other code. There is no functional
 * reason to have this branch, so do not write it. This method should only be used when you have to throw
 * the error yourself as the outcome of the subject function.
 *
 * @example
 * function f() {
 *   const 📝 = flowlogger(arguments)
 *   …
 *   throw 📝.fatal(new Error(…))
 *   …
 * }
 * @param {any} err - the error to be recorded; can be anything, but should be an {@link Error}
 * @returns {any} `err`
 */
FlowLogger.prototype.fatal = new Contract({
  pre: [
    function () {
      return !!this.flow
    }
  ],
  post: [resultIsArg]
}).implementation(function fatal (err) {
  this.log(Level.FATAL, Phase.THROWS, { err })
  return err // MUDO change to throw?
})

FlowLogger.prototype.flush = new PromiseContract({ post: [isVoid] }).implementation(async function flush () {
  return this.writer.flush()
})

FlowLogger.prototype.resetCurrent = new Contract({
  post: [
    function () {
      return FlowLogger.current === Object.getPrototypeOf(this)
    }
  ]
}).implementation(function () {
  FlowLogger.current = Object.getPrototypeOf(this)
})

FlowLogger.executionLogger = undefined
FlowLogger.current = undefined // MUDO rename to lastUsedSingleFlowLogger

module.exports = FlowLogger
