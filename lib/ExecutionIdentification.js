/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Joi = require('./_util/Joi')

/**
 * @type {ExecutionIdentification}
 */
const example1 = {
  id: 'eae484a0-d83a-45f8-bfff-7e00acc62bdb',
  hostname: 'sovereign.toryt.org',
  pid: 90364
}

/**
 * @type {ExecutionIdentification}
 */
const example2 = {
  id: 'eae484a0-d83a-45f8-bfff-7e00acc62bdb',
  hostname: '90788b70-1ef0-4a0c-ad00-7995ee891020-qnqsn', // from BitBucket
  pid: 90364
}

/**
 * Information about the current execution of the program that uses `flowlogger 𝕋流`
 *
 * @typedef ExecutionIdentification
 * @type {object}
 * @property {string} id - UUID identifying the execution
 * @property {string} hostname - hostname of the machine the current execution runs on
 * @property {number} pid - pid of the process the current execution runs in
 */

/** @type {ObjectSchema} */
const executionInformation = Joi.object()
  .keys({
    id: Joi.string()
      .uuid({
        version: ['uuidv4']
      })
      .required(),
    hostname: Joi.string()
      /* MUDO: hostname() fails for hostnames returned by BitBucket, because starts with a number.
               resolve by prepending with a rogue character?
      .hostname()
       */
      .required(),
    pid: Joi.number()
      .integer()
      .positive()
      .required()
  })
  .example(example1)
  .example(example2)

module.exports = { schema: executionInformation, example: example1 }
