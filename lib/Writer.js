/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('./_util/validateSchema')
const conditions = require('./_util/conditions')
const WriterMessage = require('./WriterMessage')

/**
 * Writers eventually write out structured log records, and can be flushed.
 */
const WriterContract = new Contract({})

WriterContract.methods = {
  /**
   * Eventually write `message` to log storage. Return asap.
   *
   * @param {WriterMessage} message - the structured log record to write to log storage; should _immediately_ be
   *                                  released by the caller after the call, and, in any case, never be changed after it
   *                                  is used as argument for this function
   * @return {void}
   */
  post: new Contract({
    pre: [validateSchema.bind(null, WriterMessage.schema.required())],
    post: [conditions.isVoid]
  }),

  /**
   * Wait until all promises are resolved or rejected.
   *
   * @return {void}
   */
  flush: new PromiseContract({
    post: [
      conditions.isVoid,
      function () {
        return this.isFlushed()
      }
    ]
  }),

  /**
   * Wait until all promises are resolved or rejected.
   *
   * @return {boolean}
   */
  isFlushed: new Contract({ post: [result => typeof result === 'boolean'] })
}

WriterContract.invariants = [
  function () {
    return WriterContract.methods.post.isImplementedBy(this.post)
  },
  function () {
    return WriterContract.methods.flush.isImplementedBy(this.flush)
  },
  function () {
    return WriterContract.methods.isFlushed.isImplementedBy(this.isFlushed)
  }
]

function WriterImpl () {
  // NOP
}

WriterImpl.prototype.constructor = WriterImpl
WriterImpl.prototype.post = WriterContract.methods.post.abstract
WriterImpl.prototype.flush = WriterContract.methods.flush.abstract
WriterImpl.prototype.isFlushed = WriterContract.methods.isFlushed.abstract
WriterImpl.prototype.invariants = WriterContract.invariants

module.exports = WriterContract.implementation(WriterImpl)
