/*
 * Copyright 2020 - 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Contract = require('@toryt/contracts-iv')
const { getFlowIdentificationContract, logFunctionProxy } = require('./logFunctionProxy')

const decorateFunctionPrototype = new Contract({}).implementation(function decorateFunctionPrototype () {
  /**
   * Mark `this` as a flow _box_, i.e., a function that collects incoming flows from the flow spring. It is a function
   * where a flow enters the execution.
   *
   * Provide a `getFlowIdentification` to determine the {@link FlowIdentification} of a new flow.
   *
   * Flow boxes should always end nominally. Any non-nominal ending is considered an {@link Phase#ERROR}.
   * This means that flow boxes should translate semantically exceptional endings into defined nominal endings.
   * The best example is an HTTP flow box, where both exceptional endings and error endings are translated in a defined
   * HTTP response, respectively with status code 4xx, or 5xx.
   *
   * Returns a Proxy.
   *
   * When the result is called,
   * - the {@link FlowIdentification} is determined by `getFlowIdentificationImplementation`
   * - a new {@link FlowLogger} is created with {@link FlowLogger#executionLogger} as prototype and the determined
   *   {@link FlowIdentification}
   * - {@link FlowLogger#current} is set to the new {@link FlowLogger}
   * - the call to the function is logged with {@link Phase#CALLED} on the new {@link FlowLogger}, with the provided
   *   `arguments`
   * - the box function is called, with the provided `arguments`, and the new {@link FlowLogger} as extra argument added
   *   as last argument.
   *
   * When the function call ends
   * - {@link FlowLogger#current} is set to the new {@link FlowLogger}
   * - if the ending is nominal
   *   - if the ending is {@link Promise}-like, the ending is logged with {@link Phase#WAITING}
   *   _ if the ending is not {@link Promise}-like, the ending and resulting value is logged with
   *     {@link Phase#RETURNED}, at level {@link Level#INFO}, {@link Level#WARN}, {@link Level#ERROR}, or
   *     {@link Level#FATAL}
   * - if the ending is non-nominal, the end and the error is logged with {@link Phase#THREW}, at level
   *   {@link Level#ERROR}, or {@link Level#FATAL}
   * - if a {@link Promise}-like result was returned, when it settles,
   *   - if the {@link Promise}-like result resolves, the resolution is logged with {@link Phase#NOMINAL},
   *     {@link Phase#EXCEPTIONAL}, {@link Phase#ERROR}, or {@link Phase#FATAL}
   *   - if the {@link Promise}-like result rejects, the rejection is logged with {@link Phase#ERROR}, or
   *     {@link Phase#FATAL}
   *
   * Whether a nominal ending is logged at level {@link Level#INFO}, {@link Level#ERROR}, or {@link Level#FATAL} is
   * determined by {@link FlowLogger#nominalEndLevel}, which is called with the resulting value of the call to `this`.
   * Whether a non-nominal ending is logged at level {@link Level#ERROR} or {@link Level#FATAL} is
   * determined by {@link FlowLogger#nonNominalEndLevel}, which is called with the value thrown by the call to `this`.
   * Both {@link FlowLogger#nominalEndLevel} and {@link FlowLogger#nonNominalEndLevel} are usually set up during the
   * initialization of `𝕋流`, but can be specialized for a particular box function with the optional arguments
   * `nominalEndPhase` and `nonNominalEndPhase`.
   *
   * @example
   * const {info, warn, debug, error, fatal} = require('@toryt@flowlogger')
   *
   * const f = function f() {
   *   …
   *   info(…)
   *   …
   *   warn(…)
   *   …
   *   debug(…)
   *   …
   *   error(…)
   *   …
   *   fatal(…)
   *   …
   * }.𝕋流box()
   *
   * @param {function} getFlowIdentificationImplementation - Will be called with the `this` and arguments the box
   *                        function is called with, and should return the {@link FlowIdentification} of the new flow to
   *                        log for. The idea is that this function extracts the {@link FlowIdentification} from the
   *                        arguments of the box function. This should be an implementation function for
   *                        {@link getFlowIdentificationContract}.
   * @param {function} [nominalEndLevel] - Optional overwrite of {@link FlowLogger#nominalEndLevel} for this box
   *                        function. Will be called with the resulting value of the call to `this` when it ends
   *                        nominally, and determine at which {@link Level} the ending is logged. Must return
   *                        {@link Level#INFO}, {@link Level#WARN}, {@link Level#ERROR}, or {@link Level#FATAL}.
   * @param {function} [nonNominalEndLevel] - Optional overwrite of {@link FlowLogger#nonNominalEndLevel} for this box
   *                        function. Will be called with the value thrown by the call to `this` when it ends
   *                        non-nominally, and determine at which {@link Level} the ending is logged. Must return
   *                        {@link Level#ERROR}, or {@link Level#FATAL}.
   * @return {function} - Proxy of `this` that logs, and adds a `𝕋logger` as last argument when calling `this`
   */
  // eslint-disable-next-line no-extend-native
  Function.prototype.𝕋流box = function 𝕋流box (
    getFlowIdentificationImplementation,
    nominalEndLevel,
    nonNominalEndLevel
  ) {
    return logFunctionProxy(
      this,
      getFlowIdentificationContract.implementation(
        getFlowIdentificationImplementation,
        nominalEndLevel,
        nonNominalEndLevel
      )
    )
  }

  /**
   * Mark `this` as a flow _duct_, i.e., a function through which a flow travels. Flow _ducts_ are finally called from
   * a flow _box_ (see {@link #𝕋流box}).
   *
   * Returns a Proxy.
   *
   * When the result is called,
   * - a new {@link FlowLogger} is created with {@link FlowLogger#current} as prototype
   * - {@link FlowLogger#current} is set to the new {@link FlowLogger}
   * - the call to the function is logged with {@link Phase#CALLED} on the new {@link FlowLogger}, with the provided
   *   `arguments`
   * - the duct function is called, with the provided `arguments`, and the new {@link FlowLogger} as extra argument
   *   added as last argument.
   *
   * When the duct function call ends
   * - {@link FlowLogger#current} is set to the new {@link FlowLogger}
   * - if the ending is nominal
   *   - if the ending is {@link Promise}-like, the ending is logged with {@link Phase#WAITING}
   *   - if the ending is not {@link Promise}-like, the ending and resulting value is logged with {@link Phase#RETURNED}
   * - if the ending is non-nominal, the end and the exception or error is logged with {@link Phase#THREW}
   * - if a {@link Promise}-like result was returned, when it settles,
   *   - if the {@link Promise}-like result resolves, the resolution is logged with {@link Phase#RETURNED}
   *   - if the {@link Promise}-like result rejects, the rejection is logged with {@link Phase#THREW}
   *
   * @example
   * const {info, warn, debug, error, fatal} = require('@toryt@flowlogger')
   *
   * const f = function f() {
   *   …
   *   info(…)
   *   …
   *   warn(…)
   *   …
   *   debug(…)
   *   …
   *   error(…)
   *   …
   *   fatal(…)
   *   …
   * }.𝕋流duct()
   *
   * @return {function} - Proxy of `this` that logs, and adds a `𝕋logger` as last argument when calling `this`
   */
  // eslint-disable-next-line no-extend-native
  Function.prototype.𝕋流duct = function 𝕋流duct () {
    return logFunctionProxy(this)
  }
})

module.exports = decorateFunctionPrototype
