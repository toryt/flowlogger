# Flowlogger 𝕋 流

**Log the flow of execution, consistently, and with minimal effort.**

> [流: stream, flux](https://translate.google.com/#view=home&op=translate&sl=zh-CN&tl=en&text=%E6%B5%81)

Especially in serverless applications, that run in the cloud, logging is of the utmost importance. It is not possible to
attach a debugger, for one, even during development. We regularly want to know exactly what happened in 1 particular
flow in production, after the facts. Finally, we often want to get metrics about the behavior and usage by end users of
the program. Which metrics are interesting often only becomes clear after the program has been in use for a while.

Consistent logging of the business decisions taken in each _flow_, and why we made them, is a functional requirement of
services in any business context. It provides an audit trail of everything that happens in the system (next to other
auditing aspects).

With `flowlogger 𝕋流` this is realized with minimal effort. With `flowlogger 𝕋流`, the path through the business logic
of the application is fully logged for each _flow_, as structured objects. Serverless environments offer the necessary
tools to analyze the logs with high expressiveness when the log records are structured objects. In other environments,
specialized tools help.

## Target environment

This application is developed for Node 12 LTS or later.

## Installation

    > npm install @toryt/flowlogger

## Usage

All programs execute 1 or more _flow_, possibly concurrently.

- There are many programs
- The same program can exist in different versions.
- Each program version can execute on different machines
- On the same machine, a program version, can be executed many times, possibly concurrently, in separate processes
- Within an execution, external events trigger one or more _flows_. Flows can be:
  - a request / response cycle, in an HTTP service or other server
  - a handling of a user event (a click, e.g.,), in a GUI application
  - a timed event, triggered by a clock
  - the only flow in an execution; this is the case, e.g., with a CLI program

### Initialization

Before you can use `flowlogger 𝕋流`, it must be initialized.

Create a module `FlowLogger.js` in your application, with the following code:

    const 𝕋flowlogger = require('@toryt/flowlogger')
    const {name, version} = require('.[…]/package.json)
    const Writer = require('@toryt/flowlogger/JSONConsoleWriter')

    const dataStructureVersion = '42.43.44'

    module.exports = 𝕋flowlogger({name, version, dataStructureVersion, new Writer()})

Note that this code will be executed once for each execution. `𝕋flowlogger()`

- stores the `Writer` instance that will be used during the execution in `FlowLogger.prototype`
- stores information identifying the execution in `FlowLogger.prototype`, i.e.,
  - UUID that will identify the execution, created by `𝕋flowlogger({name, version, writer})`
  - the program name and version, received from the arguments in `𝕋flowlogger()`
  - the semantic version of the structure of the values of the `data` property of the structured records that will be
    output during this execution
  - information identifying the machine the execution is running on, and the process the execution is running in
- gathers interesting information about the execution, and logs a structured record with that information once.

`𝕋flowlogger()` returns the function that should be used to get a `flowlogger 𝕋🔬` instance at the start of each called
business function.

The given `Writer` instance is used for all logging during the execution.

The information identifying the execution is added to all structured records logged during the execution. This includes
the semantic version of the structure of the values of the `data` property in the structured records that will be logged
during the execution.

The information that is logged about the execution at initialization is:

- the execution UUID
- `machine` — the architecture, number of CPUs, and total amount of memory of the machine (`os.arch()`,
  `os.cpus().length`, and `os.totalmem()`, resp.), and the machine's hostname (`os.hostname()`) and non-internal
  IP-addresses (`os.networkInterfaces()`)
- `os` — the type, name, and version of the OS the execution is running on (`os.type()`, `os.platform()`, and
  `os.release()`, resp.)
- `process` — the `pid` of the process the execution is running in, the `uid` and the `gid` of the process, the current
  working directory and environment variables at the time of initialization, and the command line arguments
  (`process.pid`, `process.getuid()`, `process.getgid()`, `process.cwd()`, `process.env`, `process.argv`, resp.)
- `engine` — the name, version, and LTS name (if available) of the execution engine (`process.release.name`,
  `process.release.name`, and `process.release.lts`, resp.)

Note that neither the machine's host name, nor its IP-addresses (nor its MAC addresses), can be guaranteed to uniquely
identify the machine. Note that none of the process information details is guaranteed to uniquely identify the process
in the machine. Therefor, a UUID is used to identify the execution. Yet, we do use the `hostname` and `pid` in each log
record to aid identification.

## Development

### IDE

JetBrains WebStorm settings are included and kept up-to-date by all developers.

## License

Released under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).
