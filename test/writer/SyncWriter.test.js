/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const verifyPostconditions = require('../_verifyPostconditions')
const SyncWriter = require('../../lib/writer/SyncWriter')

describe(testName, function () {
  describe('constructor', function () {
    verifyPostconditions(SyncWriter)

    it('can be constructed', function () {
      const subject = new SyncWriter()
      subject.should.upholdInvariants()
    })
  })
  describe('methods', function () {
    beforeEach(function () {
      this.subject = new SyncWriter()
    })

    afterEach(async function () {
      await this.subject.flush()
      delete this.subject
    })

    describe('#isFlushed', function () {
      verifyPostconditions('subject', 'isFlushed')

      it('is flushed initially', function () {
        this.subject.isFlushed().should.be.true()
        this.subject.should.upholdInvariants()
      })
    })

    describe('#flush', function () {
      verifyPostconditions('subject', 'flush')
      // returns Promises

      it('flush is resolved initially', async function () {
        await this.subject.flush().should.be.fulfilled()
        this.subject.should.upholdInvariants()
      })
    })
  })
})
