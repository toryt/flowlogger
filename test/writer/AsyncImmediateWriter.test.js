/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const verifyPostconditions = require('../_verifyPostconditions')
const AsyncImmediateWriter = require('../../lib/writer/AsyncImmediateWriter')
const { createMessage } = require('./Writer.common')

describe(testName, function () {
  describe('constructor', function () {
    verifyPostconditions(AsyncImmediateWriter)

    it('can be constructed', function () {
      const subject = new AsyncImmediateWriter()
      subject.should.upholdInvariants()
    })
  })
  describe('methods', function () {
    beforeEach(function () {
      this.subject = new AsyncImmediateWriter()
      this.message = createMessage(0)
      const resolvers = (this.resolvers = [])
      this.subject.write = this.subject.constructor.contract.methods.write.implementation(function writeStub (message) {
        return new Promise((resolve, reject) => {
          resolvers.push({ message, resolve, reject })
        })
      })
      this.waitForResolver = async function waitForMessage (count) {
        return new Promise(resolve => {
          const interval = setInterval(() => {
            console.log(`waiting for resolver ${count} to appear …`)
            if (resolvers.some(r => r.message.data.count === count)) {
              clearInterval(interval)
              resolve()
            }
          }, 1)
        })
      }
    })

    afterEach(async function () {
      this.resolvers.forEach(r => r.resolve())
      delete this.resolvers
      delete this.message
      await this.subject.flush()
      delete this.subject
    })

    describe('#post', function () {
      verifyPostconditions('subject', 'post')

      it('calls write, and remembers the promise', async function () {
        this.subject.isFlushed().should.be.ok()
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.not.be.ok()
        await this.waitForResolver(this.message.data.count)
        this.subject.isFlushed().should.not.be.ok()
      })
      it('deletes the remembered promise when it resolves', async function () {
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        await this.waitForResolver(this.message.data.count)
        this.subject.isFlushed().should.not.be.ok()
        this.resolvers.find(r => r.message.count === this.message.count).resolve()
        this.subject.isFlushed().should.not.be.ok() // it takes a while …
        await this.subject.flush()
        this.subject.isFlushed().should.be.ok()
      })
      it('deletes the remembered promise when it rejects', async function () {
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        await this.waitForResolver(this.message.data.count)
        this.subject.isFlushed().should.not.be.ok()
        const err = new Error('demo error')
        err.data = 'demo data'
        this.resolvers.find(r => r.message.count === this.message.count).reject(err)
        this.subject.isFlushed().should.not.be.ok() // it takes a while …
        await this.subject.flush()
        this.subject.isFlushed().should.be.ok()
      })
    })

    describe('#isFlushed', function () {
      verifyPostconditions('subject', 'isFlushed')

      it('is flushed initially', function () {
        this.subject.isFlushed().should.be.true()
        this.subject.should.upholdInvariants()
      })
      it('is not flushed with a pending write, but becomes flushed after resolve', async function () {
        this.subject.post(this.message)
        this.subject.isFlushed().should.be.false()
        this.subject.should.upholdInvariants()
        await this.waitForResolver(this.message.data.count)
        this.resolvers.find(r => r.message.count === this.message.count).resolve()
        return new Promise(resolve => {
          const interval = setInterval(() => {
            console.log('waiting for isFlushed …')
            if (this.subject.isFlushed()) {
              this.subject.should.upholdInvariants()
              clearInterval(interval)
              resolve()
            }
          }, 1)
        })
      })
      it('is not flushed with a pending write, but becomes flushed after reject', async function () {
        this.subject.post(this.message)
        this.subject.isFlushed().should.be.false()
        this.subject.should.upholdInvariants()
        await this.waitForResolver(this.message.data.count)
        const err = new Error('demo error')
        err.data = 'demo data'
        this.resolvers.find(r => r.message.count === this.message.count).reject(err)
        return new Promise(resolve => {
          const interval = setInterval(() => {
            console.log('waiting for isFlushed …')
            if (this.subject.isFlushed()) {
              this.subject.should.upholdInvariants()
              clearInterval(interval)
              resolve()
            }
          }, 1)
        })
      })
    })

    describe('#flushOnce', function () {
      verifyPostconditions('subject', 'flushOnce')

      it('flushes when there is noting to flush', async function () {
        this.subject.isFlushed().should.be.true()
        await this.subject.flushOnce().should.be.fulfilled()
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.be.true()
      })
      it('flushes when there are pending writes', async function () {
        this.subject.isFlushed().should.be.true()
        for (let i = 0; i < 5; i++) {
          this.subject.post(createMessage(i))
          this.subject.isFlushed().should.be.false()
        }
        for (let i = 0; i < 5; i++) {
          await this.waitForResolver(i)
        }
        this.resolvers.length.should.equal(5)
        // resolve or reject one by one; eventually, we get flushed
        for (let i = 0; i < 5; i++) {
          setTimeout(() => {
            if (i % 2 === 0) {
              this.resolvers[i].resolve()
            } else {
              const err = new Error('demo error')
              err.data = 'demo data'
              err.count = i
              this.resolvers[i].reject(err)
            }
          }, i * 10)
        }
        this.subject.isFlushed().should.be.false() // waiting for the setTimeouts
        await this.subject.flushOnce()
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.be.true()
      })
    })
  })
})
