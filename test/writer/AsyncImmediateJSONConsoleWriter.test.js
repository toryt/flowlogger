/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const verifyPostconditions = require('../_verifyPostconditions')
const AsyncImmediateJSONConsoleWriter = require('../../lib/writer/AsyncImmediateJSONConsoleWriter')
const { createMessage } = require('./Writer.common')
const sinon = require('sinon')

describe(testName, function () {
  describe('constructor', function () {
    verifyPostconditions(AsyncImmediateJSONConsoleWriter)

    it('can be constructed', function () {
      const subject = new AsyncImmediateJSONConsoleWriter()
      subject.should.upholdInvariants()
    })
  })
  describe('methods', function () {
    beforeEach(function () {
      this.subject = new AsyncImmediateJSONConsoleWriter()
      this.message = createMessage(0)
      this.sandbox = sinon.createSandbox()
      this.logStub = this.sandbox.spy(console, 'log')
      this.errorStub = this.sandbox.spy(console, 'error')

      this.expectNominalWrite = function () {
        this.errorStub.should.not.be.called()
        this.logStub.should.be.calledOnce()
        this.logStub.should.be.alwaysCalledWithMatch(sinon.match.string)
        const logArgJSON = this.logStub.getCall(0).args[0]
        const logArg = JSON.parse(logArgJSON)
        logArg.level = this.message.level
        logArg.phase = this.message.phase
        logArg.should.deepEqual(this.message)
      }
    })

    afterEach(async function () {
      this.sandbox.restore()
      delete this.message
      await this.subject.flush()
      delete this.subject
    })

    describe('#post - #flush - #isFlushed', function () {
      beforeEach(function () {
        this.performPost = async function () {
          this.subject.isFlushed().should.be.ok()
          this.subject.post(this.message)
          this.subject.should.upholdInvariants()
          this.subject.isFlushed().should.not.be.ok()
          await this.subject.flush().should.be.fulfilled()
          this.subject.isFlushed().should.be.ok()
        }
      })

      it('works', async function () {
        await this.performPost()
        this.expectNominalWrite()
      })
      it('works with a circular message', async function () {
        const a = { b: { c: 'c' } }
        a.b = a
        this.message.data.a = a
        await this.performPost()
        this.errorStub.should.be.calledOnce()
        this.logStub.should.not.be.called()
        // TODO check what was written instead
      })
    })

    describe('#write', function () {
      verifyPostconditions('subject', 'write')
      it('writes the JSON of a message to console with log', async function () {
        await this.subject.write(this.message).should.be.fulfilled()
        this.subject.should.upholdInvariants()
        this.expectNominalWrite()
      })
      it('balks with a circular message', async function () {
        const a = { b: { c: 'c' } }
        a.b = a
        this.message.data.a = a
        await this.subject.write(this.message).should.be.rejected()
        this.subject.should.upholdInvariants()
        this.errorStub.should.not.be.called()
        this.logStub.should.not.be.called()
      })
    })
  })
})
