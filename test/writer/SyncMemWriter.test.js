/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const verifyPostconditions = require('../_verifyPostconditions')
const SyncMemWriter = require('../../lib/writer/SyncMemWriter')
const { createMessage } = require('./Writer.common')

describe(testName, function () {
  describe('constructor', function () {
    verifyPostconditions(SyncMemWriter)

    it('can be constructed', function () {
      const subject = new SyncMemWriter()
      subject.should.upholdInvariants()
    })
  })
  describe('methods', function () {
    beforeEach(function () {
      this.subject = new SyncMemWriter()
      this.message = createMessage(0)
    })

    afterEach(async function () {
      delete this.message
      await this.subject.flush()
      delete this.subject
    })

    describe('#post', function () {
      verifyPostconditions('subject', 'post')

      it('remembers the message', function () {
        this.subject.isFlushed().should.be.ok()
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.be.ok()
        this.subject.messages.should.be.an.Array()
        this.subject.messages.length.should.equal(1)
        this.subject.messages[0].should.deepEqual(this.message)
      })
      it('remembers several messages', function () {
        const messages = []
        for (let i = 0; i < 10; i++) {
          messages.push(createMessage(i))
          this.subject.post(messages[i])
          this.subject.should.upholdInvariants()
          this.subject.isFlushed().should.be.ok()
        }
        this.subject.messages.should.be.an.Array()
        this.subject.messages.length.should.equal(10)
        this.subject.messages.every((m, i) => m.should.deepEqual(messages[i]))
      })
    })

    describe('#isFlushed', function () {
      verifyPostconditions('subject', 'isFlushed')

      it('is flushed initially', function () {
        this.subject.isFlushed().should.be.true()
      })
      it('flushed after 1 post', function () {
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.be.ok()
      })
      it('flushed after many posts', function () {
        for (let i = 0; i < 10; i++) {
          this.subject.post(createMessage(i))
          this.subject.should.upholdInvariants()
          this.subject.isFlushed().should.be.ok()
        }
      })
    })

    describe('#flush', function () {
      verifyPostconditions('subject', 'flush')
      // returns Promises

      it('flush is resolved initially', function () {
        return this.subject.flush().should.be.fulfilled()
      })
      it('flush is resolved after 1 post', function () {
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        return this.subject.flush().should.be.fulfilled()
      })
      it('flush is resolved after many posts', function () {
        const promises = []
        for (let i = 0; i < 10; i++) {
          this.subject.post(createMessage(i))
          this.subject.should.upholdInvariants()
          promises.push(this.subject.flush().should.be.fulfilled())
        }
        return Promise.all(promises)
      })
    })

    describe('#post - #flush - #isFlushed', function () {
      it('works', async function () {
        this.subject.isFlushed().should.be.ok()
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.be.ok()
        await this.subject.flush().should.be.fulfilled()
        this.subject.isFlushed().should.be.ok()
        this.subject.messages.length.should.equal(1)
        this.subject.messages[0].should.deepEqual(this.message)
      })
    })
  })
})
