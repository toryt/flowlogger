/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const verifyPostconditions = require('../_verifyPostconditions')
const SyncConsoleWriter = require('../../lib/writer/SyncConsoleWriter')
const Level = require('../../lib/Level')
const { createMessage } = require('./Writer.common')
const sinon = require('sinon')

describe(testName, function () {
  describe('constructor', function () {
    verifyPostconditions(SyncConsoleWriter)

    it('can be constructed', function () {
      const subject = new SyncConsoleWriter()
      subject.should.upholdInvariants()
    })
  })
  describe('methods', function () {
    beforeEach(function () {
      this.subject = new SyncConsoleWriter()
      this.message = createMessage(0)
    })

    afterEach(async function () {
      delete this.message
      await this.subject.flush()
      delete this.subject
    })

    describe('#post', function () {
      verifyPostconditions('subject', 'post')

      beforeEach(function () {
        this.sandbox = sinon.createSandbox()
        this.consoleSpies = {
          debug: this.sandbox.spy(console, 'debug'),
          info: this.sandbox.spy(console, 'info'),
          warn: this.sandbox.spy(console, 'warn'),
          error: this.sandbox.spy(console, 'error'),
          log: this.sandbox.spy(console, 'log'),
          dir: this.sandbox.spy(console, 'dir'),
          group: this.sandbox.spy(console, 'group'),
          groupCollapsed: this.sandbox.spy(console, 'groupCollapsed'),
          groupEnd: this.sandbox.spy(console, 'groupEnd')
        }
        this.checkSpies = function (count, method) {
          this.consoleSpies[method].should.have.been.called()
          this.consoleSpies[method].callCount.should.equal(3 * count)
          this.consoleSpies.dir.should.have.been.called()
          this.consoleSpies.dir.callCount.should.equal(count)
          this.consoleSpies.group.should.have.been.called()
          this.consoleSpies.group.callCount.should.equal(count)
          this.consoleSpies.groupCollapsed.should.have.been.called()
          this.consoleSpies.groupCollapsed.callCount.should.equal(count)
          this.consoleSpies.groupEnd.callCount.should.equal(
            this.consoleSpies.group.callCount + this.consoleSpies.groupCollapsed.callCount
          )
          // log is called by group… + empty line
          this.consoleSpies.log.callCount.should.equal(this.consoleSpies.groupEnd.callCount + count)
          otherMethods
            .filter(m => m !== method)
            .every(m => {
              this.consoleSpies[m].should.not.have.been.called()
            })
        }
      })

      const otherMethods = ['debug', 'info', 'warn', 'error']

      afterEach(function () {
        this.sandbox.restore()
      })

      const cases = [
        { level: Level.FATAL, method: 'error' },
        { level: Level.ERROR, method: 'error' },
        { level: Level.WARN, method: 'warn' },
        { level: Level.INFO, method: 'info' },
        { level: Level.DEBUG, method: 'debug' },
        { level: Level.TRACE, method: 'debug' }
      ]

      cases.forEach(c => {
        describe(`level ${c.level.toString()}`, function () {
          it(`writes the message on level ${c.level.toString()}`, function () {
            this.message.level = c.level
            this.subject.isFlushed().should.be.ok()
            this.subject.post(this.message)
            this.subject.should.upholdInvariants()
            this.subject.isFlushed().should.be.ok()
            this.checkSpies(1, c.method)
          })
          it(`writes several messages on level ${c.level.toString()}`, function () {
            for (let i = 0; i < 5; i++) {
              const message = createMessage(i)
              message.level = c.level
              this.subject.post(message)
              this.subject.should.upholdInvariants()
              this.subject.isFlushed().should.be.ok()
              this.checkSpies(i + 1, c.method)
            }
          })
        })
      })
    })

    describe('#isFlushed', function () {
      verifyPostconditions('subject', 'isFlushed')

      it('is flushed initially', function () {
        this.subject.isFlushed().should.be.true()
        this.subject.should.upholdInvariants()
      })
      it('flushed after 1 post', function () {
        this.subject.post(this.message)
        this.subject.isFlushed().should.be.ok()
        this.subject.should.upholdInvariants()
      })
      it('flushed after many posts', function () {
        for (let i = 0; i < 5; i++) {
          this.subject.post(createMessage(i))
          this.subject.isFlushed().should.be.ok()
          this.subject.should.upholdInvariants()
        }
      })
    })

    describe('#flush', function () {
      verifyPostconditions('subject', 'flush')

      it('flush is resolved initially', async function () {
        await this.subject.flush().should.be.fulfilled()
        this.subject.should.upholdInvariants()
      })
      it('flush is resolved after 1 post', async function () {
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        await this.subject.flush().should.be.fulfilled()
        this.subject.should.upholdInvariants()
      })
      it('flush is resolved after many posts', async function () {
        const promises = []
        for (let i = 0; i < 5; i++) {
          this.subject.post(createMessage(i))
          this.subject.should.upholdInvariants()
          promises.push(
            this.subject
              .flush()
              .should.be.fulfilled()
              .then(() => {
                this.subject.should.upholdInvariants()
              })
          )
        }
        await Promise.all(promises)
      })
    })

    describe('#post - #flush - #isFlushed', function () {
      it('works', async function () {
        this.subject.isFlushed().should.be.ok()
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.be.ok()
        await this.subject.flush().should.be.fulfilled()
        this.subject.isFlushed().should.be.ok()
      })
    })
  })
})
