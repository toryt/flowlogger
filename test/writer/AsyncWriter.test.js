/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const verifyPostconditions = require('../_verifyPostconditions')
const AsyncWriter = require('../../lib/writer/AsyncWriter')
const sinon = require('sinon')

const nrOfCalls = 5

describe(testName, function () {
  beforeEach(function () {
    this.sandbox = sinon.createSandbox()
    this.isFlushedStub = this.sandbox.stub()
    this.originalIsFlushed = AsyncWriter.prototype.isFlushed
    AsyncWriter.prototype.isFlushed = AsyncWriter.contract.methods.isFlushed.implementation(this.isFlushedStub)
  })

  afterEach(function () {
    AsyncWriter.prototype.isFlushed = this.originalIsFlushed
    this.sandbox.restore()
    delete this.isFlushedStub
    delete this.sandbox
  })

  describe('constructor', function () {
    verifyPostconditions(AsyncWriter)

    it('can be constructed', function () {
      this.isFlushedStub.returns(true)
      const subject = new AsyncWriter()
      subject.should.upholdInvariants()
    })
  })

  describe('methods', function () {
    beforeEach(function () {
      this.subject = new AsyncWriter()
      this.originalFlushOnce = AsyncWriter.prototype.flushOnce
      this._flushOnceStub = this.sandbox.stub().resolves()
      AsyncWriter.prototype.flushOnce = AsyncWriter.contract.methods.flushOnce.implementation(this._flushOnceStub)
    })

    afterEach(async function () {
      AsyncWriter.prototype.flushOnce = this.originalFlushOnce
      this._flushOnceStub.reset()
      delete this._flushOnceStub
      delete this.subject
    })

    describe('#flush', function () {
      verifyPostconditions('subject', 'flush')

      it('flushes when there is noting to flush', async function () {
        this.isFlushedStub.returns(true)
        await this.subject.flush()
        this.subject.should.upholdInvariants()
        this._flushOnceStub.should.not.be.called()
        this.isFlushedStub.should.be.calledTwice()
        // twice: while + postcondition; not called in invariant, because !this._pendingFlush
        // !(a && b) is optimised to (!a || !b) (De Morgan)
      })
      it("flushes when there something to flush, until there isn't", async function () {
        for (let i = 0; i < nrOfCalls; i++) {
          this.isFlushedStub.onCall(i).returns(false)
        }
        this.isFlushedStub.returns(true)
        await this.subject.flush()
        this.subject.should.upholdInvariants()
        this._flushOnceStub.callCount.should.equal(nrOfCalls - 1) // first call of isFlushedStub is in if of flush()
        this.isFlushedStub.callCount.should.equal(1 + (nrOfCalls - 1) + 1 + 1)
        // flush(), while iter, while end, postcondition; not called in invariant, because !this._pendingFlush
        // !(a && b) is optimised to (!a || !b) (De Morgan)
      })
      it('returns the same promise while a flush is pending', async function () {
        // IDEA this is an extremely difficult test; find a better way to test
        this.isFlushedStub.returns(false) // causes infinite loop
        const secondFlushDone = new Promise((resolve, reject) => {
          this._flushOnceStub.callsFake(() => {
            try {
              if (this._flushOnceStub.callCount === 3) {
                this.subject._pendingFlush.should.be.ok()
                this.subject.should.upholdInvariants() // calls isFlushed
                const secondFlush = this.subject.flush()
                // secondFlush !== firstFlush, because both are wrapped with different, extra Promises by the contract
                // function
                secondFlush.then(() => {
                  this._flushOnceStub.callCount.should.equal(nrOfCalls)
                  this.subject.should.upholdInvariants()
                  resolve()
                })
              }
              if (this._flushOnceStub.callCount >= nrOfCalls) {
                this.isFlushedStub.returns(true) // stops infinite loop, once current flush call is done
              }
              // not all invariants hold after private method
            } catch (err) {
              reject(err)
            }
          })
        })
        const firstFlush = this.subject.flush().then(() => {
          this._flushOnceStub.callCount.should.equal(nrOfCalls)
          this.subject.should.upholdInvariants()
        })
        await Promise.all([firstFlush, secondFlushDone])
        this._flushOnceStub.callCount.should.equal(nrOfCalls) // so only because of firstFlush, non because of
        // secondFlush
        this.subject.should.upholdInvariants()
        this.isFlushedStub.callCount.should.equal(2 + nrOfCalls + 1 + 2 + 1)
        // (2 x flush() if), while iter, while end, (2 x postcondition), 1 x invariant; not called in invariant the other
        // times, because !this._pendingFlush: !(a && b) is optimised to (!a || !b) (De Morgan)
      })
    })
  })
})
