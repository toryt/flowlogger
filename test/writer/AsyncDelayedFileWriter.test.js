/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const verifyPostconditions = require('../_verifyPostconditions')
const AsyncDelayedFileWriter = require('../../lib/writer/AsyncDelayedFileWriter')
const { createMessage } = require('./Writer.common')
const fs = require('fs-extra')
const x = require('cartesian')
const path = require('path')
const sinon = require('sinon')
const ExceptionConditionViolation = require('@toryt/contracts-iv/lib/IV/ExceptionConditionViolation')

const testLogDir = `test/writer/AsyncDelayedFileWriterTestLogs/${new Date().toISOString()}-${process.pid}`
const nrOfMessages = 5
const ats = ['2020-01-28T07:51:35.373391022Z', '2024-01-28T07:51:35.373391022Z', '1970-01-01T00:00:00.000000000Z']
const executionIds = [
  '130646ff-99c9-47d3-bc9a-27fda184c330',
  'e5f02d76-62cd-4b7c-adfb-e28a389a5232',
  '64d05e97-c0fc-4ef9-ae50-6a556fd39f19'
]
const flows = [
  'e229e6c8-67b3-482f-91e3-44e88b5ff182',
  'bef51284-6dcd-4faf-9cc0-8124513aed49',
  '7769e4a2-485d-41fb-b207-9fe3399b6013'
]
const cases = x({ at: ats, executionId: executionIds, flow: flows })

function prepare (subject) {
  cases.forEach(c => {
    for (let i = 0; i < nrOfMessages; i++) {
      const message = createMessage(i)
      message.at = c.at
      message.execution.id = c.executionId
      message.flow = c.flow
      subject.post(message)
    }
  })
}

describe(testName, function () {
  beforeEach(async function () {
    await fs.ensureDir(testLogDir)
    console.info(`writing test logs in ${testLogDir}`)
  })

  afterEach(async function () {
    await fs.remove(testLogDir)
  })

  describe('constructor', function () {
    verifyPostconditions(AsyncDelayedFileWriter)

    it('can be constructed', function () {
      const subject = new AsyncDelayedFileWriter(testLogDir)
      subject.should.upholdInvariants()
    })
  })
  describe('methods', function () {
    beforeEach(function () {
      this.subject = new AsyncDelayedFileWriter(testLogDir)
      this.message = createMessage(0)
    })

    afterEach(async function () {
      delete this.message
      delete this.subject
    })

    describe('#post', function () {
      verifyPostconditions('subject', 'post')

      it('works once with empty messages', function () {
        this.subject.isFlushed().should.be.ok()
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.not.be.ok()
      })
      it(`works ${nrOfMessages} times`, function () {
        this.subject.isFlushed().should.be.ok()
        for (let i = 0; i < nrOfMessages; i++) {
          this.subject.post(createMessage(i))
          this.subject.should.upholdInvariants()
          this.subject.isFlushed().should.not.be.ok()
        }
      })
      it(`works ${cases.length} times with different \`at\`, \`execution.id\`, \`flow\``, function () {
        this.subject.isFlushed().should.be.ok()
        cases.forEach((c, i) => {
          const message = createMessage(i)
          message.at = c.at
          message.execution.id = c.executionId
          message.flow = c.flow
          this.subject.post(message)
          this.subject.should.upholdInvariants()
          this.subject.isFlushed().should.not.be.ok()
        })
        cases.forEach(c => {
          const flowMessages = this.subject._messages[c.at][c.executionId][c.flow]
          flowMessages.forEach(m => {
            m.should.be.an.Object()
            m.at.should.equal(c.at)
            m.execution.id.should.equal(c.executionId)
            m.flow.should.equal(c.flow)
          })
        })
      })
    })

    // isFlushed is tested via invariants

    describe('#_messagesToWrite', function () {
      verifyPostconditions('subject', '_messagesToWrite')

      beforeEach(function () {
        this.messages = []
        for (let i = 0; i < nrOfMessages; i++) {
          this.messages.push(createMessage(i))
        }
      })

      afterEach(function () {
        delete this.messages
      })

      it('works when no file exists', async function () {
        const result = await this.subject._messagesToWrite(this.messages, 'fileDoesNotExist.json')
        result.should.deepEqual(this.messages)
      })

      describe('with a file', function () {
        beforeEach(async function () {
          this.filePath = path.format({
            dir: this.subject.logDirectoryPath,
            name: 'fileExists',
            ext: '.json'
          })
          this.fileMessages = []
          for (let i = 0; i < nrOfMessages; i++) {
            this.fileMessages.push(createMessage(i))
          }
          await fs.outputJSON(this.filePath, this.fileMessages)
        })

        afterEach(async function () {
          await fs.remove(this.filePath)
          delete this.fileMessages
          delete this.filePath
        })

        it('works when a file exists', async function () {
          const result = await this.subject._messagesToWrite(this.messages, this.filePath)
          result.should.deepEqual(this.fileMessages.concat(this.messages))
        })
      })

      describe('unexpected error during file read', function () {
        beforeEach(function () {
          this.fsReadJsonStub = sinon.stub(fs, 'readJSON')
        })

        afterEach(function () {
          this.fsReadJsonStub.restore()
        })

        it('throws when an unexpected error occurs during file read', async function () {
          // necessary for coverage, although this is considered an error
          const exc = new Error()
          this.fsReadJsonStub.rejects(exc)
          const err = await this.subject._messagesToWrite(this.messages, 'doesntMatter.json').should.be.rejected()
          err.should.be.instanceof(ExceptionConditionViolation)
          err.exception.should.equal(exc)
        })
      })
    })

    describe('#_writeFlow', function () {
      verifyPostconditions('subject', '_writeFlow')

      beforeEach(function () {
        prepare(this.subject)
      })

      it('works when messages are stringifiable', async function () {
        // since this is best effort, the existence and contents of the JSON file cannot be a postcondition
        // we test it here
        return Promise.all(
          cases.map(async c => {
            const dateString = c.at
            const originalArray = this.subject._messages[dateString][c.executionId][c.flow].slice()
            const expectedPath = path.format({
              dir: path.join(this.subject.logDirectoryPath, dateString, c.executionId),
              name: c.flow,
              ext: '.json'
            })

            await this.subject._writeFlow(dateString, c.executionId, c.flow).should.be.fulfilled()

            this.subject.should.upholdInvariants()
            const exists = await fs.pathExists(expectedPath)
            exists.should.be.true()
            const fromJson = await fs.readJSON(expectedPath)
            fromJson.should.deepEqual(originalArray)
          })
        )
      })
      describe('errors', function () {
        beforeEach(function () {
          this.consoleErrorStub = sinon.stub(console, 'error')
        })

        afterEach(function () {
          this.consoleErrorStub.restore()
        })

        it(' works when a message is not stringifiable (or another error occurs)', async function () {
          // circular structure: cannot be stringified
          const a = { b: { c: 'c' } }
          a.b.d = a
          this.message.data.a = a
          this.subject.post(this.message)

          const dateString = this.message.at
          const executionId = this.message.execution.id
          const flowId = this.message.flow
          await this.subject._writeFlow(dateString, executionId, flowId).should.be.fulfilled()

          this.subject.should.upholdInvariants()
          this.consoleErrorStub.should.be.calledOnce()
          this.consoleErrorStub.should.be.calledWith(
            sinon.match(/^Could not write flow/).and(
              sinon.match(
                path.format({
                  dir: path.join(this.subject.logDirectoryPath, dateString, executionId),
                  name: flowId,
                  ext: '.json'
                })
              ),
              sinon.match('TypeError: Converting circular structure to JSON')
            )
          )
        })
      })
    })

    describe('#_writeExecution', function () {
      verifyPostconditions('subject', '_writeExecution')

      beforeEach(function () {
        prepare(this.subject)
        this._writeFlowStub = sinon.stub(this.subject, '_writeFlow')
      })

      afterEach(function () {
        this._writeFlowStub.restore()
      })

      it('works keeps postconditions and calls `_writeFlow`', async function () {
        let expectedCallCount = 0
        await Promise.all(
          ats.reduce(
            (acc, at) =>
              acc.concat(
                executionIds.map(async executionId => {
                  const dateString = at
                  const expectedFlowIds = Object.keys(this.subject._messages[dateString][executionId])
                  expectedCallCount += expectedFlowIds.length

                  await this.subject._writeExecution(dateString, executionId)

                  this.subject.should.upholdInvariants()
                  expectedFlowIds.forEach(expectedFlowId => {
                    this._writeFlowStub.should.be.calledWith(dateString, executionId, expectedFlowId)
                  })
                })
              ),
            []
          )
        )
        this._writeFlowStub.callCount.should.equal(expectedCallCount)
      })
    })

    describe('#_writeDay', function () {
      verifyPostconditions('subject', '_writeDay')

      beforeEach(function () {
        prepare(this.subject)
        this._writeExecutionStub = sinon.stub(this.subject, '_writeExecution')
      })

      afterEach(function () {
        this._writeExecutionStub.restore()
      })

      it('works keeps postconditions and calls `_writeExecution`', async function () {
        let expectedCallCount = 0
        await Promise.all(
          ats.map(async at => {
            const dateString = at
            const expectedExecutionIds = Object.keys(this.subject._messages[dateString])
            expectedCallCount += expectedExecutionIds.length

            await this.subject._writeDay(dateString)

            this.subject.should.upholdInvariants()
            expectedExecutionIds.forEach(expectedExecutionId => {
              this._writeExecutionStub.should.be.calledWith(dateString, expectedExecutionId)
            })
          })
        )
        this._writeExecutionStub.callCount.should.equal(expectedCallCount)
      })
    })

    describe('#flushOnce', function () {
      verifyPostconditions('subject', 'flushOnce')

      beforeEach(function () {
        prepare(this.subject)
        this._writeExecutionStub = sinon.stub(this.subject, '_writeExecution')
      })

      afterEach(function () {
        this._writeExecutionStub.restore()
      })

      it('works keeps postconditions and calls `_writeDay`', async function () {
        const expectedCalls = Object.keys(this.subject._messages).reduce(
          (acc, dateString) =>
            Object.keys(this.subject._messages[dateString]).reduce((acc, executionId) => {
              acc.push({ dateString, executionId })
              return acc
            }, acc),
          []
        )

        await this.subject.flushOnce()

        this.subject.should.upholdInvariants()
        this._writeExecutionStub.callCount.should.equal(expectedCalls.length)
        expectedCalls.forEach(call => {
          this._writeExecutionStub.should.be.calledWith(call.dateString, call.executionId)
        })
      })
    })

    describe('#post - #flush - #isFlushed', function () {
      it('works', async function () {
        this.subject.isFlushed().should.be.ok()
        this.subject.post(this.message)
        this.subject.should.upholdInvariants()
        this.subject.isFlushed().should.not.be.ok()
        await this.subject.flush().should.be.fulfilled()
        this.subject.isFlushed().should.be.ok()
      })
    })
  })
})
