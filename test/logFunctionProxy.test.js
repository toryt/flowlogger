/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const {
  createLogger,
  getFlowIdentificationContract,
  apply,
  construct,
  logFunctionProxy
} = require('../lib/logFunctionProxy')
const flowlogger = require('../lib')
const SyncMemWriter = require('../lib/writer/SyncMemWriter')
const FlowLogger = require('../lib/FlowLogger')
const FlowIdentification = require('../lib/FlowIdentification')
const sinon = require('sinon')
const Phase = require('../lib/Phase')
const should = require('should')
const Level = require('../lib/Level')

const getFlowIdentification = getFlowIdentificationContract.implementation(() => '9ae4170b-fe40-4d4f-8d93-b1d8e13a7e61')
const functionName = 'this is a function name'

describe(testName, function () {
  beforeEach(function () {
    delete FlowLogger.current
    delete FlowLogger.executionLogger
    this.writer = new SyncMemWriter()
    flowlogger({
      programName: 'example program',
      programVersion: '42.42.42',
      logDataVersion: '123.124.125',
      writer: this.writer
    })
    this.baseFlowLogger = Object.create(FlowLogger.executionLogger, {
      flow: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: FlowIdentification.example
      }
    })
    FlowLogger.current = this.baseFlowLogger

    this.self = {}
    this.args = [1, 2, 3]
    this.expectedResult = {}
    this.flowLoggerCurrent = undefined
  })

  afterEach(function () {
    delete FlowLogger.current
    delete FlowLogger.executionLogger
  })

  describe('createLogger', function () {
    beforeEach(function () {
      createLogger.contract.verifyPostconditions = true
    })
    afterEach(function () {
      createLogger.contract.verifyPostconditions = false
    })

    it('creates an initial flow logger with a flowIdentification', function () {
      const flowId = '4816a4aa-f349-4a04-a022-319502fb2161'
      const result = createLogger(functionName, flowId)
      result.should.equal(FlowLogger.current)
      result.should.be.instanceOf(FlowLogger)
      result.flow.should.equal(flowId)
      result.function.should.equal(functionName)
    })
    it('creates a function call flow logger without a flowIdentification', function () {
      const result = createLogger(functionName)
      result.should.equal(FlowLogger.current)
      result.should.be.instanceOf(FlowLogger)
      result.flow.should.equal(FlowIdentification.example)
      result.function.should.equal(functionName)
    })
  })

  describe('apply', function () {
    function generate (getFlowIdentification) {
      beforeEach(function () {
        this.subject = sinon.stub()
        this.flowId = getFlowIdentification ? getFlowIdentification() : FlowLogger.current.flow
        this.expectedBaseLogger = getFlowIdentification ? FlowLogger.executionLogger : FlowLogger.current

        this.expectPostApply = function (noArgs) {
          this.flowLoggerCurrent.should.be.instanceOf(FlowLogger)
          Object.getPrototypeOf(this.flowLoggerCurrent).should.equal(this.expectedBaseLogger)
          this.writer.messages.length.should.equal(3)
          this.writer.messages[1].level.should.equal(Level.INFO)
          this.writer.messages[1].phase.should.equal(Phase.CALLED)
          should(this.writer.messages[1].data.arguments).deepEqual(noArgs ? undefined : this.args)
          this.subject.should.be.calledOnce()
          this.subject.should.be.calledOn(noArgs ? undefined : this.self)
          if (noArgs) {
            this.subject.should.be.calledWithExactly()
          } else {
            this.subject.should.be.calledWithExactly(...this.args)
          }

          FlowLogger.current.should.equal(this.expectedBaseLogger)
        }
        this.expectPostApplyNominal = function (result, noArgs) {
          this.expectPostApply(noArgs)
          this.writer.messages[2].level.should.equal(Level.INFO)
          this.writer.messages[2].phase.should.equal(Phase.RETURNS)
          this.writer.messages[2].data.result.should.equal(this.expectedResult)
          result.should.equal(this.expectedResult)
        }

        this.expectPostApplySettled = function () {
          this.flowLoggerCurrent.should.be.instanceOf(FlowLogger)
          Object.getPrototypeOf(this.flowLoggerCurrent).should.equal(this.expectedBaseLogger)
          this.writer.messages.length.should.equal(4)
          this.subject.should.be.calledOnce()
          FlowLogger.current.should.equal(this.expectedBaseLogger)
        }
      })

      it('works with a sync function with nominal outcome', function () {
        this.subject.callsFake(() => {
          this.flowLoggerCurrent = FlowLogger.current
          return this.expectedResult
        })

        const result = apply(getFlowIdentification, this.subject, this.self, this.args)

        this.expectPostApplyNominal(result)
      })
      it('works with a sync function with a nominal outcome without arguments', function () {
        this.subject.callsFake(() => {
          this.flowLoggerCurrent = FlowLogger.current
          return this.expectedResult
        })

        const result = apply(getFlowIdentification, this.subject)

        this.expectPostApplyNominal(result, true)
      })
      it('works with a sync function that throws', function () {
        this.subject.callsFake(() => {
          this.flowLoggerCurrent = FlowLogger.current
          throw this.expectedResult
        })

        apply
          .bind(undefined, getFlowIdentification, this.subject, this.self, this.args)
          .should.throw(this.expectedResult)

        this.expectPostApply()
        this.writer.messages[2].level.should.equal(Level.INFO)
        this.writer.messages[2].phase.should.equal(Phase.THROWS)
        this.writer.messages[2].data.thrown.should.equal(this.expectedResult)
      })
      it('works with an async function that resolves', async function () {
        this.subject.callsFake(() => {
          this.flowLoggerCurrent = FlowLogger.current
          return Promise.resolve(this.expectedResult)
        })

        const resultPromise = apply(getFlowIdentification, this.subject, this.self, this.args)

        resultPromise.should.be.a.Promise()
        this.expectPostApply()
        this.writer.messages[2].level.should.equal(Level.INFO)
        this.writer.messages[2].phase.should.equal(Phase.WAITING)

        const result = await resultPromise

        this.expectPostApplySettled()
        this.writer.messages[3].level.should.equal(Level.INFO)
        this.writer.messages[3].phase.should.equal(Phase.RETURNS)
        this.writer.messages[3].data.result.should.equal(this.expectedResult)
        result.should.equal(this.expectedResult)
      })
      it('works with an async function that rejects', async function () {
        this.subject.callsFake(() => {
          this.flowLoggerCurrent = FlowLogger.current
          return Promise.reject(this.expectedResult)
        })

        const resultPromise = apply(getFlowIdentification, this.subject, this.self, this.args)

        resultPromise.should.be.a.Promise()
        this.expectPostApply()
        this.writer.messages[2].level.should.equal(Level.INFO)
        this.writer.messages[2].phase.should.equal(Phase.WAITING)

        await resultPromise.should.be.rejectedWith(this.expectedResult)

        this.expectPostApplySettled()
        this.writer.messages[3].level.should.equal(Level.INFO)
        this.writer.messages[3].phase.should.equal(Phase.THROWS)
        this.writer.messages[3].data.thrown.should.equal(this.expectedResult)
      })
    }

    describe('without `getFlowIdentification`', function () {
      generate()
    })
    describe('with `getFlowIdentification`', function () {
      generate(getFlowIdentification)
    })
  })

  describe('constructor', function () {
    // NOTE: cannot use sinon stubs, nor spies, with a constructor in this case (it calls `new` on Subject itself)
    function generate (getFlowIdentification) {
      beforeEach(function () {
        const fixture = this

        fixture.prototype = {}
        fixture.callCount = 0
        fixture.self = undefined
        fixture.arguments = undefined
        fixture.shouldThrow = false

        fixture.Subject = function Subject () {
          fixture.callCount++
          fixture.flowLoggerCurrent = FlowLogger.current
          fixture.self = this
          fixture.arguments = Array.from(arguments)
          if (fixture.shouldThrow) {
            throw fixture.expectedResult
          }
          this.property = fixture.expectedResult
        }

        fixture.Subject.prototype = fixture.prototype
        fixture.prototype.constructor = fixture.Subject

        fixture.flowId = getFlowIdentification ? getFlowIdentification() : FlowLogger.current.flow
        fixture.expectedBaseLogger = getFlowIdentification ? FlowLogger.executionLogger : FlowLogger.current

        fixture.expectPostConstructor = function (noArgs) {
          fixture.flowLoggerCurrent.should.be.instanceOf(FlowLogger)
          Object.getPrototypeOf(fixture.flowLoggerCurrent).should.equal(fixture.expectedBaseLogger)
          fixture.writer.messages.length.should.equal(3)
          fixture.writer.messages[1].level.should.equal(Level.INFO)
          fixture.writer.messages[1].phase.should.equal(Phase.CALLED)
          should(fixture.writer.messages[1].data.arguments).deepEqual(noArgs ? undefined : fixture.args)
          fixture.callCount.should.equal(1)
          if (noArgs) {
            fixture.arguments.should.deepEqual([])
          } else {
            fixture.arguments.should.deepEqual(fixture.args)
          }
          FlowLogger.current.should.equal(fixture.expectedBaseLogger)
        }
        fixture.expectPostConstructorNominal = function (result, noArgs) {
          this.expectPostConstructor(noArgs)
          this.writer.messages[2].level.should.equal(Level.INFO)
          this.writer.messages[2].phase.should.equal(Phase.RETURNS)
          this.writer.messages[2].data.result.should.equal(this.self)
          result.should.equal(this.self)
          Object.getPrototypeOf(result).should.equal(this.prototype)
        }
      })

      it('works with nominal outcome', function () {
        const result = construct(getFlowIdentification, this.Subject, this.args)

        this.expectPostConstructorNominal(result)
      })
      it('works with a nominal outcome without arguments', function () {
        const result = construct(getFlowIdentification, this.Subject)

        this.expectPostConstructorNominal(result, true)
      })
      it('works with a function that throws', function () {
        this.shouldThrow = true
        construct.bind(undefined, getFlowIdentification, this.Subject, this.args).should.throw(this.expectedResult)

        this.expectPostConstructor()
        this.writer.messages[2].level.should.equal(Level.INFO)
        this.writer.messages[2].phase.should.equal(Phase.THROWS)
        this.writer.messages[2].data.thrown.should.equal(this.expectedResult)
      })
    }

    describe('without `getFlowIdentification`', function () {
      generate()
    })
    describe('with `getFlowIdentification`', function () {
      generate(getFlowIdentification)
    })
  })

  describe('logFunctionProxy', function () {
    function generate (getFlowIdentification) {
      beforeEach(function () {
        logFunctionProxy.contract.verifyPostconditions = true
        this.flowId = getFlowIdentification ? getFlowIdentification() : FlowLogger.current.flow
        this.expectedBaseLogger = getFlowIdentification ? FlowLogger.executionLogger : FlowLogger.current
      })
      afterEach(function () {
        logFunctionProxy.contract.verifyPostconditions = false
      })

      it('returns a proxy when called on a regular function', function () {
        const flowId = this.flowId
        function subject () {
          FlowLogger.current.flow.should.equal(flowId)
        }
        const subjectSpy = sinon.spy(subject)

        const result = logFunctionProxy(subjectSpy, getFlowIdentification)
        // cannot express that result[[[Target]]] === f, because Symbol [[Target]] is private, so call and test that way
        result()
        subjectSpy.should.have.been.calledOnce()
      })
      it('returns a proxy when called on a constructor', function () {
        const flowId = this.flowId
        function Subject () {
          FlowLogger.current.flow.should.equal(flowId)
        }
        const prototype = { aProperty: 'a property', constructor: Subject }
        Subject.prototype = prototype
        const SubjectSpy = sinon.spy(Subject)

        const result = logFunctionProxy(SubjectSpy, getFlowIdentification)
        result.prototype.should.equal(prototype)
        result.prototype.constructor.should.equal(result)
        // cannot express that result[[[Target]]] === f, because Symbol [[Target]] is private, so call and test that way
        result()
        SubjectSpy.should.have.been.calledOnce()
      })
    }

    describe('without `getFlowIdentification`', function () {
      generate()
    })
    describe('with `getFlowIdentification`', function () {
      generate(getFlowIdentification)
    })
  })

  describe('proxy', function () {
    function generate (getFlowIdentification) {
      beforeEach(function () {
        this.flowId = getFlowIdentification ? getFlowIdentification() : FlowLogger.current.flow
        this.expectedBaseLogger = getFlowIdentification ? FlowLogger.executionLogger : FlowLogger.current
      })

      it('proxies a regular function with intercepts', function () {
        const fixture = this
        const expectedResult = {}
        function subject (𝕋) {
          Object.getPrototypeOf(FlowLogger.current).should.equal(fixture.expectedBaseLogger)
          FlowLogger.current.flow.should.equal(fixture.flowId)
          return expectedResult
        }

        const proxied = logFunctionProxy(subject, getFlowIdentification)

        const result = proxied()
        result.should.equal(expectedResult)
        proxied.name.should.equal('subject')
        FlowLogger.current.should.equal(fixture.expectedBaseLogger)
      })
      it('proxies a constructor with intercepts, with a prototype on the constructor', function () {
        const fixture = this
        const expectedResult = {}
        function Subject (𝕋) {
          Object.getPrototypeOf(FlowLogger.current).should.equal(fixture.expectedBaseLogger)
          FlowLogger.current.flow.should.equal(fixture.flowId)
          this.aProperty = expectedResult
        }

        Subject.prototype.constructor = Subject
        Subject.prototype.aPrototypeProperty = 'a prototype property value'

        const Proxied = logFunctionProxy(Subject, getFlowIdentification)

        Proxied.prototype.aPrototypeProperty.should.equal(Subject.prototype.aPrototypeProperty)

        const result = new Proxied()
        result.aPrototypeProperty.should.equal(Subject.prototype.aPrototypeProperty)
        result.aProperty.should.equal(expectedResult)

        result.should.be.instanceOf(Proxied)
        result.should.be.instanceOf(Subject)

        Proxied.prototype.constructor.should.equal(Proxied)
        // Subject.prototype.constructor.should.equal(Subject)
        FlowLogger.current.should.equal(fixture.expectedBaseLogger)
      })
      it('proxies a constructor with intercepts, with a prototype on the Proxy', function () {
        const fixture = this
        const expectedResult = {}
        function Subject () {
          Object.getPrototypeOf(FlowLogger.current).should.equal(fixture.expectedBaseLogger)
          FlowLogger.current.flow.should.equal(fixture.flowId)
          this.aProperty = expectedResult
        }

        const Proxied = logFunctionProxy(Subject, getFlowIdentification)

        Proxied.prototype.constructor = Proxied
        Proxied.prototype.aPrototypeProperty = 'a prototype property value'
        Proxied.prototype.aPrototypeProperty.should.equal(Subject.prototype.aPrototypeProperty)

        const result = new Proxied()

        result.aPrototypeProperty.should.equal(Subject.prototype.aPrototypeProperty)
        result.aProperty.should.equal(expectedResult)

        Proxied.prototype.constructor.should.equal(Proxied)
        result.should.be.instanceOf(Proxied)
        result.should.be.instanceOf(Subject)
        FlowLogger.current.should.equal(fixture.expectedBaseLogger)
      })
      it('works with an async', async function () {
        const fixture = this
        const expectedResult = {}
        async function subject () {
          Object.getPrototypeOf(FlowLogger.current).should.equal(fixture.expectedBaseLogger)
          FlowLogger.current.flow.should.equal(fixture.flowId)
          return Promise.resolve(expectedResult)
        }

        const proxied = logFunctionProxy(subject, getFlowIdentification)

        const result = await proxied()
        result.should.equal(expectedResult)
        proxied.name.should.equal('subject')
        FlowLogger.current.should.equal(fixture.expectedBaseLogger)
      })
    }

    describe('without `getFlowIdentification`', function () {
      generate()
    })
    describe('with `getFlowIdentification`', function () {
      generate(getFlowIdentification)
    })
  })
})
