/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const isArguments = require('../../lib/_util/isArguments')
const util = require('util')

describe(testName, function () {
  const circular = { a: { b: { c: 'c' } } }
  circular.a.b.d = circular.a.b
  circular.a.b.c.e = [1, 2, circular, 4]
  const cases = [
    null,
    undefined,
    3,
    0,
    Math.E,
    Number.NEGATIVE_INFINITY,
    true,
    false,
    '',
    'a string',
    [],
    [1, 2, 3, 4],
    {},
    { a: 'a' },
    { a: { b: { c: 'c' } } },
    circular,
    new Date(2020, 0, 12, 17, 23, 27, 844),
    JSON,
    Math,
    (function () {
      return arguments
    })(),
    (function () {
      delete arguments.length
      return arguments
    })(),
    (function () {
      // noinspection JSValidateTypes
      arguments.length = 'not a number'
      return arguments
    })(),
    (function () {
      arguments.length = Math.E
      return arguments
    })(),
    (function () {
      arguments.length = -12
      return arguments
    })()
  ]

  beforeEach(function () {
    isArguments.contract.verifyPostconditions = true
  })

  afterEach(function () {
    isArguments.contract.verifyPostconditions = false
  })

  cases.forEach(c => {
    it(`works as expected for ${util.inspect(c)}`, function () {
      const result = isArguments(c)
      console.log(`result: ${result}`)
    })
  })
})
