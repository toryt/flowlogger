/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const should = require('should')
const semver = require('semver')
const { conciseCondition } = require('@toryt/contracts-iv/lib/_private/report')

should.Assertion.add('semver', function () {
  this.params = { operator: 'to be a semantic version' }
  should(this.obj).be.ok()
  this.obj.should.be.a.String()
  semver.valid(this.obj).should.not.be.null()
})

should.Assertion.add('containAll', function (expectedElements) {
  if (!Array.isArray(expectedElements)) {
    this.params = { operator: "to be compared with parameter of 'containAll', which must be an array" }
    this.fail()
  }
  this.params = { operator: `to contain all elements [${expectedElements.join(', ')}]` }
  expectedElements.forEach(e => {
    this.obj.should.containEql(e)
  })
})

should.Assertion.add('upholdInvariant', function (invar) {
  this.params = { operator: `to uphold invariant ${conciseCondition('', invar)}` }
  invar.should.be.a.Function()
  invar.call(this.obj).should.be.ok()
})

should.Assertion.add('upholdInvariants', function () {
  this.params = { operator: 'to uphold invariants' }
  if (this.obj.constructor.contract.invariants) {
    this.obj.invariants.should.be.an.Array()
    this.obj.invariants.should.containAll(this.obj.constructor.contract.invariants)
  }
  if (this.obj.invariants) {
    this.obj.invariants.should.be.an.Array()
    this.obj.invariants.forEach(invar => {
      this.obj.should.upholdInvariant(invar)
    })
  }
})
