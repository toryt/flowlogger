/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const /** @type {ContractFunction,function} */ FlowLogger = require('../lib/FlowLogger')
const SyncMemWriter = require('../lib/writer/SyncMemWriter')
const Level = require('../lib/Level')
const Phase = require('../lib/Phase')
const FlowIdentification = require('../lib/FlowIdentification')
const sinon = require('sinon')
const nano = require('nano-seconds')

function createSubjectKwargs () {
  return {
    programName: 'test program',
    programVersion: '1.2.3',
    logDataVersion: '4.5.6',
    writer: new SyncMemWriter()
  }
}

describe(testName, function () {
  describe('current', function () {
    it('has a property current', function () {
      FlowLogger.should.have.ownProperty('current')
    })
  })
  describe('constructor', function () {
    beforeEach(function () {
      FlowLogger.contract.verifyPostconditions = true
    })

    afterEach(function () {
      FlowLogger.contract.verifyPostconditions = false
    })

    it('should uphold invariants', function () {
      const result = new FlowLogger(createSubjectKwargs())
      result.should.upholdInvariants()
    })
  })
  describe('instance methods', function () {
    beforeEach(function () {
      this.oldCurrent = FlowLogger.current
      this.subject = new FlowLogger(createSubjectKwargs())
      // noinspection JSUnresolvedVariable
      FlowLogger.current = this.subject
    })

    afterEach(function () {
      delete this.subject
      // noinspection JSUnresolvedVariable
      FlowLogger.current = this.oldCurrent
    })

    describe('#log', function () {
      it(`works when phase != ${Phase.INITIALIZED}`, function () {
        const data = {}
        const expectedMessage = {
          level: Level.DEBUG,
          // at determined by log
          '𝕋流': this.subject['𝕋流'],
          program: { ...this.subject.program },
          execution: { ...this.subject.execution },
          flow: FlowIdentification.example,
          phase: Phase.RETURNS,
          function: 'functionName',
          data: { ...data }
        }
        this.subject.flow = expectedMessage.flow
        this.subject.function = expectedMessage.function
        this.subject.log(expectedMessage.level, expectedMessage.phase, data)
        this.subject.should.upholdInvariants()
        this.subject.writer.messages.length.should.equal(1)
        expectedMessage.at = this.subject.writer.messages[0].at
        this.subject.writer.messages[0].should.deepEqual(expectedMessage)
      })
      it(`works when phase = ${Phase.INITIALIZED}`, function () {
        const data = {}
        const expectedMessage = {
          level: Level.DEBUG,
          // at determined by log
          '𝕋流': this.subject['𝕋流'],
          program: { ...this.subject.program },
          execution: { ...this.subject.execution },
          flow: FlowIdentification.example,
          phase: Phase.INITIALIZED,
          function: undefined,
          data: { ...data }
        }
        this.subject.flow = expectedMessage.flow
        this.subject.log(expectedMessage.level, expectedMessage.phase, data)
        this.subject.should.upholdInvariants()
        this.subject.writer.messages.length.should.equal(1)
        expectedMessage.at = this.subject.writer.messages[0].at
        this.subject.writer.messages[0].should.deepEqual(expectedMessage)
      })
    })

    function describeVoidInstanceMethod (methodName, argFactory) {
      describe(`#${methodName}`, function () {
        beforeEach(function () {
          FlowLogger.prototype[methodName].contract.verifyPostconditions = true
        })

        afterEach(function () {
          FlowLogger.prototype[methodName].contract.verifyPostconditions = false
        })

        it('should reach its postconditions', function () {
          this.subject.flow = FlowIdentification.example
          this.subject[methodName](argFactory())
          this.subject.should.upholdInvariants()
          // MUDO test message
        })
      })
    }

    describeVoidInstanceMethod('initialized', function () {
      return arguments
    })
    describeVoidInstanceMethod('called', function () {
      return arguments
    })
    describeVoidInstanceMethod('selection', () => 'a selection')
    describeVoidInstanceMethod('iteration', () => ({ iterationData: 'iterationData' }))

    function describeOutcomeInstanceMethod (methodName) {
      describe(`#${methodName}`, function () {
        beforeEach(function () {
          FlowLogger.prototype[methodName].contract.verifyPostconditions = true
        })

        afterEach(function () {
          FlowLogger.prototype[methodName].contract.verifyPostconditions = false
        })

        it('should reach its postconditions', function () {
          const aResult = {}
          this.subject.flow = FlowIdentification.example
          const result = this.subject[methodName](aResult)
          this.subject.should.upholdInvariants()
          result.should.equal(aResult)
          // MUDO test message
        })
      })
    }

    describeOutcomeInstanceMethod('returns')
    describeOutcomeInstanceMethod('throws')
    describeOutcomeInstanceMethod('error')
    describeOutcomeInstanceMethod('fatal')

    describe('flush', function () {
      beforeEach(function () {
        this.sandbox = sinon.createSandbox()
        this.writerFlushStub = this.sandbox.stub(this.subject.writer, 'flush')
        // cannot spy on contract method because of https://github.com/sinonjs/sinon/issues/2203, but can stub
      })

      afterEach(function () {
        this.sandbox.restore()
      })

      it("calls the writer's #flush", async function () {
        await this.subject.flush()
        this.writerFlushStub.should.be.calledOnce()
      })
    })
  })
  describe('nano-seconds', function () {
    it('returns ns since epoch', function () {
      for (let i = 0; i < 30; i++) {
        const nowNs = nano.now()
        console.log(nowNs)
        const iso = nano.toISOString()
        console.log(iso)
        const dFromIso = new Date(iso)
        console.log(dFromIso.toISOString())
      }
    })
  })
})
