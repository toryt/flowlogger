const flowlogger = require('../../lib')
const SyncConsoleWriter = require('../../lib/writer/SyncConsoleWriter')

const executionFlowLogger = flowlogger({
  programName: 'example program',
  programVersion: '42.42.42',
  logDataVersion: '123.124.125',
  writer: new SyncConsoleWriter()
})

module.exports = executionFlowLogger
