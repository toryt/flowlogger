require('../../lib/decorateFunctionPrototype')()

const functionB1 = function functionB1 (argument1, argument2) {}.𝕋流duct()

const functionB2 = function functionB2 () {}.𝕋流duct()

const functionB3 = function functionB3 () {}.𝕋流duct()

module.exports = {
  functionB1,
  functionB2,
  functionB3
}
