const FlowLogger = require('../../lib/FlowLogger')
const { v4: uuidv4 } = require('uuid')
const module2 = require('./module2')
require('./myFlowLogger')
require('../../lib/decorateFunctionPrototype')()

const topFunction = function topFunction (argument1, argument2, argument3) {
  functionA1(argument1, argument2)
  functionA2(argument3)
  functionA3()
}.𝕋流box(() => uuidv4())

const functionA1 = function functionA1 (argument1, argument2) {
  if (argument2 === 'a') {
    FlowLogger.current.selection("argument2 === 'a'")
    module2.functionB1()
  } else {
    FlowLogger.current.selection("argument2 !== 'a'")
    module2.functionB2()
  }
}.𝕋流duct()

const functionA2 = function functionA2 () {
  arguments[0].forEach(a => {
    FlowLogger.current.iteration(a)
    module2.functionB2(a)
    module2.functionB2(a)
  })
}.𝕋流duct()

const functionA3 = function functionA3 () {
  module2.functionB3()
}.𝕋流duct()

module.exports = {
  topFunction,
  functionA1,
  functionA2,
  functionA3
}
