/*
 * Copyright 2020 - 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
require('../lib/decorateFunctionPrototype')
const sinon = require('sinon')

describe(testName, function () {
  it('adds 𝕋流box and 𝕋流duct to Function.prototype', function () {
    function subject () {}

    subject.𝕋流box.should.be.a.Function()
    subject.𝕋流duct.should.be.a.Function()
  })
  it('𝕋流box() returns a logging Function', function () {
    let called
    function subject () {
      called = true
    }

    const box = subject.𝕋流box(() => '2eb6ae27-aca8-4faa-b8d8-7ecf43165880')
    box.should.be.a.Function()
    box()
    called.should.be.true()
  })
  it('𝕋流duct() returns a logging Function', function () {
    let called
    function subject () {
      called = true
    }

    const duct = subject.𝕋流duct()
    duct.should.be.a.Function()
    duct()
    called.should.be.true()
  })
  it('𝕋流box() works with sinon stub', function () {
    const stub = sinon.stub()

    const box = stub.𝕋流box(() => '2eb6ae27-aca8-4faa-b8d8-7ecf43165880')
    box.should.be.a.Function()
    box()
    stub.should.be.calledOnce()
  })
  it('𝕋流duct() works with sinon stub', function () {
    const stub = sinon.stub()

    const duct = stub.𝕋流duct(() => '2eb6ae27-aca8-4faa-b8d8-7ecf43165880')
    duct.should.be.a.Function()
    duct()
    stub.should.be.calledOnce()
  })
  it('𝕋流box() works with sinon spy', function () {
    let called
    function subject () {
      called = true
    }
    const originalSpy1 = sinon.spy(subject)
    const originalSpy2 = sinon.spy(subject)

    const box = originalSpy1.𝕋流box(() => '2eb6ae27-aca8-4faa-b8d8-7ecf43165880')
    box.should.be.a.Function()
    const boxSpy = sinon.spy(box)
    boxSpy()
    originalSpy1.should.be.calledOnce()
    originalSpy2.should.not.be.called()
    boxSpy.should.be.calledOnce()
    called.should.be.true()
  })
  it('𝕋流duct() works with sinon spy', function () {
    let called
    function subject () {
      called = true
    }
    const originalSpy1 = sinon.spy(subject)
    const originalSpy2 = sinon.spy(subject)

    const duct = originalSpy1.𝕋流duct()
    duct.should.be.a.Function()
    const boxSpy = sinon.spy(duct)
    boxSpy()
    originalSpy1.should.be.calledOnce()
    originalSpy2.should.not.be.called()
    boxSpy.should.be.calledOnce()
    called.should.be.true()
  })
})
