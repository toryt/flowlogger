/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const ExecutionIdentification = require('../lib/ExecutionIdentification')
const expectSeriousSchema = require('./_expectSeriousSchema')

const nonStrings = [undefined, null, '', 8, false, [], {}, Symbol('s')]
const nonPostives = [undefined, null, '', false, [], {}, Symbol('s'), -8, 0, Math.E]
const invalids = nonStrings
  .filter(ns => ns !== undefined)
  .concat(nonStrings.map(ns => ({ ...ExecutionIdentification.example, id: ns })))
  .concat(nonStrings.map(ns => ({ ...ExecutionIdentification.example, hostname: ns })))
  .concat(nonPostives.map(ns => ({ ...ExecutionIdentification.example, pid: ns })))
  .concat([
    {
      ...ExecutionIdentification.example,
      id: 'not a uuid'
    }
  ])
/* MUDO: hostname() fails for hostnames returned by BitBucket, because starts with a number.
  .concat([
    {
      ...ExecutionIdentification.example,
      hostname: '𝕋流 not a host name'
    }
  ])
*/

describe(testName, function () {
  expectSeriousSchema(ExecutionIdentification.schema, invalids)
})
