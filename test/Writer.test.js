/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const verifyPostconditions = require('./_verifyPostconditions')
const Writer = require('../lib/Writer')

describe(testName, function () {
  describe('constructor', function () {
    verifyPostconditions(Writer)

    it('can be constructed', function () {
      const subject = new Writer()
      subject.should.upholdInvariants()
    })
  })
})
