/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */
/**
 * Returns the {@link ContractFunction} `contractFunction`. This is `contractfunction` itself if
 * it is a `function`, or `this[contractFunction]` when it is a `string`.
 *
 * @param {(ContractFunction|string)} contractFunction
 * @param {string} [methodName]
 * @return {ContractFunction}
 */
function cf (contractFunction, methodName) {
  // prettier-ignore
  return typeof contractFunction === 'function'
    ? contractFunction
    : !methodName
      ? this[contractFunction]
      : this[contractFunction][methodName]
}

/**
 * Set `contractFunction.contract.verifyPostconditions = true` before each test,
 * and reset after each test.
 *
 * `contractFunction` is either the contract function, or the name of the property in the test fixture that holds the
 * subject contract function.
 *
 * @param {(ContractFunction|string)} contractFunction
 * @param {string} [methodName]
 */
function verifyPostconditions (contractFunction, methodName) {
  let originalValue

  beforeEach(function () {
    const contract = cf.call(this, contractFunction, methodName).contract
    originalValue = contract.verifyPostconditions
    contract.verifyPostconditions = true
  })

  afterEach(function () {
    cf.call(this, contractFunction, methodName).contract.verifyPostconditions = originalValue
  })
}

module.exports = verifyPostconditions
