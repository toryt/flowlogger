/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const HrTime = require('../lib/HrTime')
const expectSeriousSchema = require('./_expectSeriousSchema')

const stuff = [null, '', 8, false, [], {}, Symbol('s')]
const wrongStrings = [
  '+2020-01-28T04:28:48.797167311Z',
  '-2020-01-28T04:28:48.797167311Z',
  '20-01-28T04:28:48.797167311Z',
  '2020-01-28T04:28:48,797167311Z',
  '2020-01-28T04:28,48.797167311Z',
  '2020-01-28T04,28:48.797167311Z',
  '2020-01-28T04:28:48.79716731184884Z',
  '2020-01-28T04:28:48.797167311CET',
  '2020-01-28T04:28:48.797167311',
  '2020-01-28T04:28:48',
  '2020-1-28T04:28:48.797167311Z',
  '2020-13-28T04:28:48.797167311Z',
  '2020-100-28T04:28:48.797167311Z',
  '2020-01-32T04:28:48.797167311Z',
  '2019-02-100T04:28:48.797167311Z',
  '2020-01-28T24:28:48.797167311Z',
  '2020-01-28T32:28:48.797167311Z',
  '2020-01-28T124:28:48.797167311Z',
  '2020-01-28T04:61:48.797167311Z',
  '2020-01-28T04:101:48.797167311Z',
  '2020-01-28T04:28:61.797167311Z',
  '2020-01-28T04:28:101.797167311Z'
]

/* The current pattern does not flag as wrong impossible days in February:
  '2020-02-30T04:28:48.797167311Z'
  '2019-02-29T04:28:48.797167311Z' */

describe(testName, function () {
  expectSeriousSchema(HrTime.schema, stuff.concat(wrongStrings))
})
